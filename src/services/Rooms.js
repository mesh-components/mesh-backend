/**
 * справочник кабинетов
 */
export class Rooms {
	/**
	 * @ngInject
	 */
	constructor(Core) {
		this.services = {Core};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/rooms");
	}
}