import _ from "main/app.mixins";

class NewsCategories {
	/**
	 * @ngInject
	 */
	constructor($q, Argus) {
		this.services = {$q, Argus};
	}

	api() {
		const {Argus} = this.services;

		return Argus.service("/news/categories");
	}

	/**
	 * Возвращает список категорий новостей
	 */
	getCategories() {
		return this
			.api()
			.getList();
	}
}

export {NewsCategories};