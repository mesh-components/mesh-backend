/**
 * Формы контроля для оценок внеурочной деятельности
 */
export class ECControlForms {
	/**
	 * @ngInject
	 */
	constructor(Core) {
		this.services = {Core};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/ec_control_forms");
	}
}