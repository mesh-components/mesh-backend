class ProgressReport {
	/**
	 * @ngInject
	 */
	constructor(Reports) {
		this.services = {Reports};
	}

	api() {
		const {Reports} = this.services;

		return Reports.service("/new_progress/json");
	}
}

export {ProgressReport};