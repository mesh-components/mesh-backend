class TargetGroups {
	/**
	 * @ngInject
	 */
	constructor(Argus, BigQuery) {
		this.services = {Argus, BigQuery};
	}

	api() {
		const {Argus} = this.services;

		return Argus.service("/targeting_groups");
	}

	getAll(params={}) {
		const {BigQuery} = this.services;

		return BigQuery.getList(this.api(), params, 100);
	}
}

export {TargetGroups};