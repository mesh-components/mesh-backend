import _ from "main/app.mixins";

import iconMale from "images/icons/i-m.svg";
import iconFemale from "images/icons/i-w.svg";
import iconAll from "images/icons/i-mw.svg";

export class TargetingCategoryFactory {
    constructor() {
        this.dataTemplate = {
            educationLevels: [],
            type: "staff",
            roles: [],
            subjects: [],
            classLevels: [],
            counties: [],
            schools: [],
            gender: ["male", "female"],
            childAgeFrom: null,
            childAgeTo: null,
            childGender: ["male", "female"],
            ageFrom: null,
            ageTo: null
        };
        this.categoriesTypes = [
            {
                type: "staff",
                title: "сотрудники",
                educationLevels: [1, 2, 3, 4] // id уровней образования, для которых доступна категория
            },
            {
                type: "parent",
                title: "родители",
                educationLevels: [1, 2, 3, 4]
            },
            {
                type: "student",
                title: "учащиеся",
                educationLevels: [1, 2, 3]
            }
        ];
        this.roleGroups = [
            {
                name: "Воспитатели",
                groupType: "educator",
                educationLevels: [4],
                roles: ["educator"]
            },
            {
                name: "Учителя",
                groupType: "teacher",
                educationLevels: [1, 2, 3],
                roles: ["teacher"]
            },
            {
                name: "Классные руководители",
                groupType: "mentor",
                educationLevels: [1, 2, 3],
                roles: ["mentor"]
            },
            {
                name: "Педагогические работники",
                groupType: "pedagog",
                educationLevels: [1, 2, 3],
                roles: ["principal", "deputy", "psychologist", "methodologist", "curator", "medal_commission", "passport_printer"]
            },
            {
                name: "Администрация оо",
                groupType: "admin",
                educationLevels: [4],
                roles: ["admin"]
            },
            {
                name: "Администраторы ЭЖД",
                groupType: "adminEzd",
                educationLevels: [4],
                roles: ["school_admin_read_only", "school_admin"]
            },
            {
                name: "Разработчики контента",
                groupType: "copywriter",
                educationLevels: [1, 2, 3, 4],
                roles: ["copywriter"]
            },
            {
                name: "Все сотрудники",
                groupType: "staff",
                educationLevels: [4],
                roles: ["staff"]
            }
        ];
        this.genders = [
            {
                type: "all",
                title: "Любой",
                icon: iconAll,
                value: ["male", "female"]
            },
            {
                type: "male",
                title: "Мужчины",
                icon: iconMale,
                value: ["male"]
            },
            {
                type: "female",
                title: "Женщины",
                icon: iconFemale,
                value: ["female"]
            }
        ];
    }

    get(data = {}) {
        const newCategory = _.assign({}, this.dataTemplate, data);

        newCategory.educationLevels = _.find(this.categoriesTypes, {type: newCategory.type}).educationLevels;

        return newCategory;
    }

    getLevelsIntersection(type, levels) {
        const category = _.find(this.categoriesTypes, {type});

        return _.intersection(category.educationLevels, levels);
    }

    getGenders() {
        return _.clone(this.genders, true);
    }

    getGenderByValue(valueArr) {
        if (!valueArr || !valueArr.length || valueArr.length === this.genders.length - 1) {
            return _.find(this.genders, {type: "all"});
        }

        return _.find(this.genders, (gender) => _.isEqual(gender.value, valueArr));
    }

    getGroupTitleForRole(role) {
        const group = _.find(this.roleGroups, (item) => _.intersects(item.roles, [role]));

        return _.get(group, "name", "");
    }

    getGenderTitles(values) {
        return _.chain(this.genders)
            .filter((item) => _.includes(values, item.type))
            .map("title")
            .value()
            .join(", ");
    }
}