class TargetTemplates {
	/**
	 * @ngInject
	 */
	constructor(Argus, BigQuery) {
		this.services = {Argus, BigQuery};
		this.TARGET_TEMPLATE_TEMPLATE = {
			name: null,
			// массив с категориями охвата, заданными для новости.
			// Для одной новости категорий может быть несколько,
			targetCategories: []
		};
	}

	api() {
		const {Argus} = this.services;

		return Argus.service("/target_templates");
	}

	getAll(params={}) {
		const {BigQuery} = this.services;

		return BigQuery.getList(this.api(), params, 100);
	}

	getTemplate() {
		return _.clone(this.TARGET_TEMPLATE_TEMPLATE, true);
	}

	cleanTemplateObject(template) {
		const systemFields = ["id", "infoData", "createdDate", "updatedDate", "isAutoCreated", "aprilWeight", "augustWeight",
			"decemberWeight", "februaryWeight", "novemberWeight", "octoberWeight", "septemberWeight", "januaryWeight",
			"julyWeight", "juneWeight", "marchWeight", "mayWeight"];
		_.forEach(systemFields, (field) => {
			delete template[field];
		});

		_.forEach(template.targetCategories, (category) => {
			delete category.id;
		});
	}
}

export {TargetTemplates};