import _ from "main/app.mixins";

export class BigQuery {
	/**
	 * @ngInject
	 */
	constructor(Restangular) {
		this.services = {Restangular};
	}

	getList(provider, data, parts = 50, reqType = "GET") {
		const {Restangular} = this.services;
		const perPage = parts || 50;
		let page = 1;
		let result = [];

		const methods = {
            GET: "getList",
			POST: "post"
		};

		const reqMethod = _.get(provider, methods[reqType], () => []);

		return worker();

		function worker() {
			const chunk = _.cloneDeep(data);

			chunk.page = page;
			chunk.per_page = perPage;

			return reqMethod(chunk)
				.then((res) => {
					result = _.union(result, res.plain());

					const totalPages = _.int(res.headers("Pages") || res.headers("X-Pagination-Total-Pages"));

					if (totalPages && !_.isNaN(totalPages) && page === totalPages) {
						return _.map(result, (item) => Restangular.restangularizeElement("", item, res.getRestangularUrl()));
					}

					page += 1;

					if (_.isEmpty(res)) {
						return _.map(result, (item) => Restangular.restangularizeElement("", item, res.getRestangularUrl()));
					}

					return worker();
				})
				.catch(() => []);
		}
	}

	getListByChunkedParams(provider, data, chunkField, parts = 50) {
		const {Restangular} = this.services;

		if (_.isEmpty(data[chunkField])) {
			return [];
		}

		const chunks = _.chunk(data[chunkField].split(","), parts);

		delete data[chunkField];

		return worker([]);

		function worker(result) {
			let $result = result;
			const chunk = chunks.shift();
			const chunkedData = _.cloneDeep(data);

			chunkedData[chunkField] = chunk.join(",");

			return provider
				.getList(chunkedData)
				.then((res) => {
					$result = _.union($result, res.plain());

					if (chunks.length > 0) {
						return worker($result);
					}

					return _.map($result, (item) => Restangular.restangularizeElement("", item, res.getRestangularUrl()));
				})
				.catch(() => []);
		}
	}
}