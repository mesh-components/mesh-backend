import _ from "main/app.mixins";

/**
 * Профили учителей
 */
export class TeacherProfiles {
	/**
	 * @ngInject
	 */
	constructor(Core) {
		this.services = {Core};

		// расширяем модель
		Core.extendModel("/teacher_profiles", (model) => {
			model.getFullName = this.getFullName;

			return model;
		});
	}

	api() {
		const {Core} = this.services;

		return Core.service("/teacher_profiles");
	}

	getFullName() {
		// Формат профиля учителя, возвращаемого $$lessonReplacement.getAutocompleteTeachers
		if (this.fullName) {
			return this.fullName;
		}

		// виртуальный учитель
		if (this.name || this.full_name) {
			return this.name || this.full_name;
		}

		// обычный учитель
		const teacherName = [
			this.last_name || "",
			this.first_name || "",
			this.middle_name || ""
		];

		return teacherName.join(" ");
	}

	async getPage(page, filter) {
		const {Core} = this.services;
		const result = {
			data: [],
			pages: 1
		};
		const rest = Core.withConfig((RestangularConfigurer) => RestangularConfigurer.setFullResponse(true));
		let params = {page, per_page: 50};

		if (_.keys(filter).length !== 0) {
			params = angular.extend(params, filter);
		}

		return await rest.all("/teacher_profiles")
			.getList(params)
			.then((response) => {
				result.data = response.data;
				result.pages = _.int(response.headers("X-Pagination-Total-Pages")) || 1;
				result.total_entries = _.int(response.headers("X-Pagination-Total-Entries")) || 0;

				return result;
			});
	}
}