/**
 * Формы контроля для оценок доп образования
 */
export class AEControlForms {
	/**
	 * @ngInject
	 */
	constructor(Core) {
		this.services = {Core};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/ae_control_forms");
	}
}