/**
 * рабочий календарь
 */
export class Calendars {
	/**
	 * @ngInject
	 */
	constructor(Core) {
		this.services = {Core};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/calendars");
	}

	/**
	 * Возвращает рабочий календарь на год
	 *
	 * @param calendarId {Number} id календаря учебного года
	 */
	getYearCalendar(calendarId) {
		const {Core} = this.services;

		return Core
			.one("/calendars", calendarId)
			.all("calendar_transpositions")
			.getList();
	}
}