class Subjects {
	/**
	 * @ngInject
	 */
	constructor(Argus, BigQuery, $q) {
		this.services = {Argus, BigQuery, $q};
	}

	api() {
		const {Argus} = this.services;

		return Argus.service("/school_dictionaries/subjects");
	}

	getAll(params={}) {
		const {BigQuery} = this.services;

		return BigQuery.getList(this.api(), params, 100);
	}

    /**
     * Загрузка предметов по уровням образования
     *
     * @returns {*}
     */
    async loadSubjectsByEducationLevels(educationLevels) {
        const {$q} = this.services;
        // загружаем предметы по каждому из уровней образования
        const subjectsProms = _.map(educationLevels, async (level) => {
            const subjectsByLevel = await this.getAll({education_level_ids: level.id});
            _.forEach(subjectsByLevel, (subject) => {
                // присваиваем предмету id уровня образования
                subject.levelId = level.id;
            });

            return subjectsByLevel;
        });

        const subjectsByLevels = await (this.promise = $q.all(subjectsProms));
        // объединяем списки полученных предметов
        const allSubjectsWithLevels = _.flatten(subjectsByLevels);

        // выделяем предметы с уникальным id и мапим на них списки уровней образования
        return _.chain(allSubjectsWithLevels).uniq("id").map((subject) => {
            const subjectLevelIds = _.chain(allSubjectsWithLevels).filter({id: subject.id}).map("levelId").value();
            subject.$educationLevelsLabel = _.chain(educationLevels)
                .filter((level) => _.includes(subjectLevelIds, level.id))
                .map((level) => `(${level.short_title})`)
                .value()
                .join(", ");

            return subject;
        }).value();
    }
}

export {Subjects};