import _ from "main/app.mixins";

import allIcon from "images/news/all_news.svg";
import teacherIcon from "images/news/teacher.svg";
import parentIcon from "images/news/parent.svg";
import studentIcon from "images/news/student.svg";
import archiveIcon from "images/news/archive.svg";


class TargetCategories {
	constructor() {
		this.categories = [
			/*			all: {
			 title: "Все",
			 shortTitle: "Все",
			 superShortTitle: "Все",
			 icon: allIcon,
			 color: "",
			 params: {
			 type: "all"
			 }
			 },*/
			{
				title: "Для сотрудников сферы образования",
				shortTitle: "Для сотрудников",
				superShortTitle: "Сотрудники",
				icon: teacherIcon,
				color: "#93618d",
				params: {
					type: "staff"
				}
			},
			{
				title: "Для родителей",
				shortTitle: "Для родителей",
				superShortTitle: "Родители",
				icon: parentIcon,
				color: "#729460",
				params: {
					type: "parent"
				}
			},
			{
				title: "Для учащихся",
				shortTitle: "Для учащихся",
				superShortTitle: "Учащиеся",
				icon: studentIcon,
				color: "#917661",
				params: {
					type: "student"
				},
				details: [
					{
						title: "5-8",
						params: {
							type: "student",
							target_class_levels: [5, 6, 7, 8]
						}
					},
					{
						title: "9-11",
						params: {
							type: "student",
							target_class_levels: [9, 10, 11]
						}
					}
				]
			}/*,
			 archive: {
			 title: "Архив",
			 shortTitle: "Архив",
			 icon: archiveIcon,
			 color: "",
			 params: {
			 type: "archive",
			 is_archive: true
			 }
			 }*/
		];
	}

	getTargetingCategories(diffs) {
		let result = _.clone(this.categories, true);

		if (Boolean(diffs)) {
			_.forIn(diffs, (val, key) => {
				_.assign(result[key], val);
			});
		}

		return result;
	}
}

export {TargetCategories};