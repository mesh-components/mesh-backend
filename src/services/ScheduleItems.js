import _ from "main/app.mixins";

/**
 * Уроки
 */
export class ScheduleItems {

	/**
	 * @ngInject
	 */
	constructor($rootScope, Jersey, MeschSessions, UserProfiles) {
		this.services = {$rootScope, Jersey, MeschSessions, UserProfiles};
		this.lessonTypes = {
			THEMATIC_TEST: {
				type: "THEMATIC_TEST",
				title: "Контрольная работа",
				shortTitle: "кр"
			},
			NORMAL: {
				type: "NORMAL",
				title: "",
				shortTitle: ""
			},
			MODULE_TEST: {
				type: "MODULE_TEST",
				title: "Промежуточная аттестация",
				shortTitle: "па"
			},
			PRACTICE: {
				type: "PRACTICE",
				title: "Практическая работа",
				shortTitle: "пр"
			},
			LAB: {
				type: "LAB",
				title: "Лабораторная работа",
				shortTitle: "лр"
			}
		};
	}

	api() {
		const {Jersey} = this.services;

		return Jersey.service("/schedule_items");
	}

	/**
	 * Возвращает текстовое описание типа урока
	 *
	 * @param lesson {*}
	 * @returns {*}
	 */
	getLessonType(lesson) {
		const lessonType = _.get(lesson, "course_lesson_type", "NORMAL");

		return _.clone(this.lessonTypes[lessonType]);
	}

	/**
	 * TODO
	 * @returns {Array}
	 */
	getAttachments(lesson) {
		return [];
	}

	/**
	 * TODO
	 * @returns {Array}
	 */
	getTests(lesson) {
		return [];
	}

	/**
	 * Ссылки на кэсы
	 *
	 * @returns {*}
	 */
	getControllableItems(lesson) {
		const {$rootScope, MeschSessions, UserProfiles} = this.services;
		const profileId = _.get(UserProfiles.currentProfile, "id", "");
		const userId = _.get(UserProfiles.currentProfile, "user_id", "");
		const token = MeschSessions.getToken();
		const subjectId = _.get(lesson, "subject_id");
		const items = _.get(lesson, "controllable_items", []) || [];
		const baseUrl = $rootScope.isProd ? "https://uchebnik.mos.ru" : "https://uchebnik-stable.mos.ru";

		return _.map(items, (item) => {
			return {
				type: "controllable-item",
				name: `${item.code} ${item.name}`,
				url: "#"
				// url: `${baseUrl}/ui/teacher/content-library?section=search&controllable_item_id=${item.id}&subject_id=${subjectId}&type=lesson_templates&user_id=${userId}&profile_id=${profileId}&token=${token}`
			};
		});
	}

	getScripts(lesson) {
		const scripts = _.get(lesson, "scripts", "");

		try {
			const fromString = JSON.parse(scripts);

			return _.map(fromString, this.$getScriptUrl.bind(this));
		} catch (e) {
			return [];
		}
	}

	$getScriptUrl(script) {
		const {MeschSessions, $rootScope, UserProfiles} = this.services;
		const profileId = _.get(UserProfiles.currentProfile, "id", "");
		const userId = _.get(UserProfiles.currentProfile, "user_id", "");
		const token = MeschSessions.getToken();
		const baseUrl = $rootScope.isProd ? "https://uchebnik.mos.ru" : "https://uchebnik-stable.mos.ru";

		return {
			url: `${baseUrl}/authenticate?authToken=${token}&profileId=${profileId}&userId=${userId}&backurl=${baseUrl}/composer2/lesson/${script.material_id}/view`,
			name: script.name,
			type: "script"
		};
	}
}