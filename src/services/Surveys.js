import _ from "main/app.mixins";
import "moment/min/locales.js";

class Surveys {
    /**
     * @ngInject
     */
    constructor(Survey, BigQuery, TargetingCategoryFactory, TargetCategories, UrsNotification, $timeout, $state) {
        this.services = {
            Survey,
            BigQuery,
            TargetingCategoryFactory,
            TargetCategories,
            UrsNotification,
            $timeout,
            $state
        };
        this.targetCategories = TargetCategories.getTargetingCategories() || [];
        this.fullApi = Survey.withConfig((RestangularConfigurer) => RestangularConfigurer.setFullResponse(true));
        this.url = "/survey";
    }

    api() {
        const {Survey} = this.services;

        return Survey.service(this.url);
    }

    customApi(url = this.url) {
        return this.fullApi.all(url);
    }

    getAll(params = {}) {
        const {BigQuery, Survey} = this.services;

        return BigQuery.getList(Survey.service("/survey/list"), params, 100, "POST");
    }

    getTemplate() {
        const {TargetingCategoryFactory} = this.services;
        const surveyTemplate = {
            publishDate: "", // дата публикации опроса DD.MM.YYYY
            publishPeriod: 30, // период публикации. количество дней публикации начиная с даты публикации. По-умолчанию 30 дней.
            title: "", // заголовок опроса
            detail: "", // текст опроса
            questions: [], // вопросы опроса
            externalSystemsIds: [],
            timeLimit: null,
            previewImage: null,
            attachmentIds: [],
            targetTemplate: {
                name: null,
                aprilWeight: 1,
                augustWeight: 1,
                decemberWeight: 1,
                februaryWeight: 1,
                januaryWeight: 1,
                julyWeight: 1,
                juneWeight: 1,
                marchWeight: 1,
                mayWeight: 1,
                novemberWeight: 1,
                octoberWeight: 1,
                septemberWeight: 1,
                // массив с категориями охвата, заданными для опроса. Для одного опроса категорий может быть несколько,
                targetCategories: _.map(this.targetCategories, (category) => TargetingCategoryFactory.get({type: category.params.type}))
            }
        };

        return _.clone(surveyTemplate, true);
    }

    /**
     * Загрузка списка опросов
     */
    getPage(num = 1, perPage, params) {
        const completeParams = _.assign(params, {page: num, per_page: perPage});

        return this.customApi("/survey/list")
            .post(completeParams)
            .then((response) => {
                const result = {
                    items: response.data,
                    pageNum: _.int(response.headers("X-Pagination-Page")) || 1,
                    pagesTotal: _.int(response.headers("X-Pagination-Total-Pages")) || 0,
                    itemsTotal: _.int(response.headers("X-Pagination-Total-Items")) || 0
                };

                return result;
            });
    }

    /**
     * Запускает всплывающие уведомления об опросах
     * */
    async startNotification() {
        // // не даем всплывать окошку, если мы уже на экране опросов
        // if ($state.current.name === "surveys.passing") {
        //     return;
        // }
        //
        // if (UrsNotification.isShowed) {
        //     return;
        // }

        this.surveysList = await this.getAll({is_published: true});

        // if (this.surveysList.length) {
        //     let notificationHTML;
        //     let notificationType = (surveysList.length > 1) ? "small" : "big";
        //     if (this.surveysList.length > 1) {
        //         notificationHTML =
        //             `<div class="sm survey-notification">
        //                 <div>За последнее время стартовало несколько опросов. Приглашаем вас принять участие!</div>
        //                 ${_.map(this.surveysList, (survey) => {
        //                 return `<a href="/surveys/passing/${survey.id}"
        //                                class="survey-passing-link">«${survey.title}»</a>`;
        //             }).join("")}
        //             </div>`;
        //     } else {
        //         notificationHTML =
        //             `<div class="sm survey-notification">
        //                 <div>Стартовал опрос «${this.surveysList[0].title}»!</div>
        //                 <a href="/surveys/passing/${this.surveysList[0].id}">
        //                     <div class="button blue-button">Принять участие</div>
        //                 </a>
        //             </div>`;
        //     }
        //
        //     $timeout(() => {
        //         UrsNotification.add(notificationType, notificationHTML, true);
        //     }, 3000);
        //
        //     UrsNotification.isShowed = true;
        // }
    }

    /**
     * Возвращает статус опроса: черновик, опубликованная, архив, новая
     */
    getSurveyState(survey) {
        if (!Boolean(survey.id)) {
            return "new";
        }
        // если не задана дата публикации, то новость считается черновиком
        if (!Boolean(survey.publishDate)) {
            return "draft";
        }
        // если у новости выставлен флаг архивности, то новость считается архивной
        if (survey.isArchived) {
            return "archive";
        }

        const publishPeriod = _.int(survey.publishPeriod) || 1;
        const publishDateStart = moment(survey.publishDate).startOf("day");
        const publishDateEnd = moment(survey.publishDate).add(publishPeriod, "days");
        const today = moment().startOf("day");

        // если период публикации закончился, то новость считается архивной
        if (publishDateEnd.isBefore(today)) {
            return "archive";
        }

        // если дата публикации новости наступила, но не закончился период публикации,
        // то новость считается опубликованной
        if (publishDateStart.isBefore(today) || publishDateStart.isSame(today)) {
            return "published";
        }

        // если дата публикации не наступила, то новость считается черновиком

        return "draft";
    }

    moveToArchive(survey) {
        survey.isArchived = true;
        survey = survey.clone();

        return survey.put();
    }

    unpublishSurvey(surveyId) {
        return this.api().one("unpublish").one(surveyId).put();
    }

    getDaysLeft(survey) {
        const endDate = moment(survey.publishDate).add(survey.publishPeriod, "days");
        const daysLeft = Math.abs(moment().diff(endDate, "days"));

        return `Остал${daysLeft % 10 === 1 ? "ся" : "ось"} ${daysLeft} ${getDaysDeclination.call(this, daysLeft)}`;

        function getDaysDeclination(number) {
            const titles = ["день", "дня", "дней"];

            return this.getDeclination(number, titles);
        }
    }

    getDeclination(period, titles) {
        const cases = [2, 0, 1, 1, 1, 2];
        const title = titles[(period % 100 > 4 && period % 100 < 20) ? 2 : cases[(period % 10 < 5) ? period % 10 : 5]];

        return title;
    }

    copySurvey(surveyId) {
        return this.api().one("copy").one(surveyId).get();
    }
}

export {Surveys};