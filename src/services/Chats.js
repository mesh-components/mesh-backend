class Chats {
    /**
     * @ngInject
     */
    constructor(Chat, BigQuery, CookiesStorage, AUTH_TOKEN_FIELD_NAME, AUTH_TOKEN_HEADER_NAME) {
        this.services = {Chat, BigQuery, CookiesStorage};
        this.tokenFieldName = AUTH_TOKEN_FIELD_NAME;
        this.tokenHeaderName = AUTH_TOKEN_HEADER_NAME;

        // расширяем модель
        Chat.extendModel("/chats", (model) => {
            model.isGroupChat = this.isGroupChat;

            return model;
        });
    }

    customApi() {
        const {Chat} = this.services;

        return Chat.all("/chats");
    }

    api() {
        const {Chat} = this.services;

        return Chat.service("/chats");
    }

    /**
     * Загрузка всех чатов в соответствии с переданными параметрами
     */
    getAll(params = {}) {
        const {BigQuery} = this.services;

        return BigQuery.getList(this.api(), params, 50);
    }

    /**
     * Шаблон нового чата
     */
    getTemplate() {
        return {
            name: "",
            author_profile_id: null,
            is_hidden: false,
            profile_ids: []
        };
    }

    customPUT(chat) {
        const {CookiesStorage} = this.services;
        const headers = {
            [this.tokenHeaderName]: CookiesStorage.get(this.tokenFieldName),
            "Profile-Id": CookiesStorage.get("profileId")
        };

        return chat.customPUT(null, null, null, headers);
    }

    customDELETE(chat) {
        return this.customApi().customDELETE(chat.id);
    }

    isGroupChat() {
        const profileIds = _.get(this, "profile_ids") || [];

        return profileIds.length > 2;
    }
}

export {Chats};