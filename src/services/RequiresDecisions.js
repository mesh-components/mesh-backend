/**
 * Сервис для доступа к данным требующим решения.
 */
export class RequiresDecisions {
    /**
     * @ngInject
     */
    constructor(Core, $rootScope, MeschSessions, $interval, UserProfiles) {
        this.services = {Core, $rootScope, MeschSessions, $interval, UserProfiles};
    }

    api() {
        const {Core} = this.services;

        return Core.service("/requires_decisions");
    }

    /**
     * Получает количество пунктов, требующих решения
     * @returns {PromiseLike<T> | Promise<T>}
     */
    getRequiresDecisionsCount() {
        return this
            .api()
            .one("count")
            .get()
            .then((data) => {
                this.count = data.count;
            });
    }

    /**
     * Проверяет, есть ли пункты, требующие решения
     */
    checkRequiresDecisionsCount() {
        const {$interval} = this.services;

        this.getRequiresDecisionsCount();
        $interval(this.getRequiresDecisionsCount.bind(this), 60000);
    }

    async openEZDRequiresDecisions() {
        const {UserProfiles, MeschSessions, $rootScope} = this.services;

        if (this.count === 0) {
            return;
        }

        const baseUrl = $rootScope.isProd ? "https://dnevnik.mos.ru" : "http://test.ezd.altarix.org";
        const token = MeschSessions.getToken();
        const currentProfile = await UserProfiles.getCurrentProfile();
        const stateToGo = currentProfile.type === "teacher"
            ? "journal_checks"
            : "students_without_curricula";

        window.open(`${baseUrl}?token=${token}&backurl=/decisions/${stateToGo}`, "_blank");
    }
}