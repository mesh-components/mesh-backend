import _ from "main/app.mixins";

/**
 * справочник канцелярских принадлежностей для рюкзака школьника
 */
export class Stationery {
	/**
	 * @ngInject
	 */
	constructor(Core, $q, BigQuery) {
		this.services = {Core, $q, BigQuery};
		this.items = [];
		this.listIsLoading = false;
	}

	api() {
		const {Core} = this.services;

		return Core.service("/stationery");
	}

	loadList() {
		const {$q} = this.services;

		// если список не загружается и он пуст, то пробуем загрузить еще раз
		if (!this.listIsLoading && _.isEmpty(this.items)) {
			return this.$loadBooks();
		}
		// если список загружается, то ждем когда загрузится
		if (this.listIsLoading) {
			return $q((resolve) => {
				const t = setInterval(() => {
					if (!this.listIsLoading) {
						clearInterval(t);
						resolve(angular.copy(this.items));
					}
				}, 200);
			});
		}
		// список не загружается и не пустой

		return $q((resolve) => {
			resolve(angular.copy(this.items));
		});
	}

	$loadBooks() {
		const {BigQuery} = this.services;

		this.listIsLoading = true;

		return BigQuery
			.getList(this.api(), {}, 50)
			.then((data) => {
				this.items = data || [];
				this.listIsLoading = false;

				return data;
			})
			.catch(() => {
				this.listIsLoading = false;

				return [];
			});
	}
}