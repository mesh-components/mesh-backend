import _ from "main/app.mixins";
import moment from "main/moment-local";

/**
 * Домашние задания для учеников
 */
export class StudentHomeworks {
	/**
	 * @ngInject
	 */
	constructor($q, $rootScope, Core, Restangular, MeschSessions, Attachments, BigQuery, CookiesStorage, UrsAlert, AUTH_TOKEN_FIELD_NAME, AUTH_TOKEN_HEADER_NAME) {
		this.services = {
			$q,
			$rootScope,
			Core,
			Restangular,
			MeschSessions,
			Attachments,
			BigQuery,
			CookiesStorage,
			UrsAlert,
			AUTH_TOKEN_FIELD_NAME,
			AUTH_TOKEN_HEADER_NAME
		};
		this.tokenFieldName = AUTH_TOKEN_FIELD_NAME;
		this.tokenHeaderName = AUTH_TOKEN_HEADER_NAME;

		// расширяем модель
		Core.extendModel("/student_homeworks", (model) => {
			model.getHumanDuration = this.getHumanDuration;

			return model;
		});
	}

	api() {
		const {Core} = this.services;

		return Core.service("/student_homeworks");
	}

	customApi() {
		const {Core} = this.services;

		return Core.all("/student_homeworks");
	}

	/**
	 * Загрузка всех домашних заданий ученика с учетом параметров запроса
	 *
	 * @param params {*} параметры фильтрации списка дз
	 * @param errorMessage {String} текст сообщения об ошибке
	 */
	loadAll(params, errorMessage = "") {
		const {BigQuery, $q, UrsAlert} = this.services;

		// оборачивание в промис нужно, чтобы резолвить даже в случае ошибки.
		// костыль нужен для $q.all, так как в случае реджекта одного промиса, реджектится общий промис
		//
		// https://docs.angularjs.org/api/ng/service/$q
		// Returns a single promise that will be resolved with an array/hash of values, each value corresponding
		// to the promise at the same index/key in the promises array/hash. If any of the promises is resolved
		// with a rejection, this resulting promise will be rejected with the same rejection value.
		return $q((resolve) => {
			BigQuery
				.getList(this.api(), params, 50)
				.then((response) => {
					resolve(response);
				})
				.catch(() => {
					if (errorMessage) {
						UrsAlert.error(errorMessage, true);
					}
					resolve([]);
				});
		});
	}

	/**
	 * Давным давно в далекой-далекой галактике в описании домашнего задания была захардкожена json строка с ссылками
	 * на учебники Магия в этом методе достает эту строку и парсит в массив объектов
	 *
	 * @param homeworkDescription {String} - описание домашнего задания
	 *
	 * @returns {Array}
	 */
	getUrlsFromDescription(homeworkDescription) {
		let links;
		let jsonString;

		try {
			jsonString = homeworkDescription.split("JSON::")[1] || "[]";
			links = JSON.parse(jsonString);

			return _.map(links, this.$getBookUrl.bind(this));
		}
		catch (e) {
			return [];
		}
	}

	/**
	 * Возвращает текст, в котором все ссылки обернуты в <a>
	 *
	 * @param rawText {String}
	 * @returns {String}
	 */
	getWrappedUrls(rawText) {
		if (!rawText) {
			return "";
		}
		// регулярное выражение для поиска ссылок
		const regexp = /((https?:\/\/)?[\w-]+(\.[a-z-]+)+\.?(:\d+)?(\/\S*)?)/gim;

		return rawText.replace(regexp, (url) => {
			let fullUrl = url;

			if (!fullUrl.match(/^https?:\/\//)) {
				fullUrl = `http://${fullUrl}`;
			}

			return `<a href="${fullUrl}" target="_blank">${url}</a>`;
		});
	}

	getAttachments(homework) {
		const attachments = _.get(homework, "homework_entry.attachments", []);

		return _.map(attachments, (item) => {
			return this.getAttachmentItem(item);
		});
	}


	getAttachmentItem(item) {
		const {Attachments, $rootScope} = this.services;

		if (!item) {
			return null;
		}

		const fileName = _.get(item, "file_file_name", "");
		const simpleName = fileName.split(".");
		const extension = (simpleName.length > 1) ? simpleName.pop() : "";
		const baseUrl = $rootScope.isProd ? "https://dnevnik.mos.ru" : "http://test.ezd.altarix.org";

		item.title = fileName;
		item.name = simpleName;
		item.extension = extension ? `.${extension}` : "";
		item.url = `${baseUrl}${item.path}`;
		item.size = Attachments.getFileSizeString(item.file_file_size);
		item.type = "file";

		return item;
	}

	getScripts(homework) {
		const scripts = _.get(homework, "homework_entry.scripts", null);

		try {
			const fromScripts = JSON.parse(scripts);

			return _.map(fromScripts, this.$getScriptUrl.bind(this));
		} catch (e) {
			return [];
		}
	}

	/**
	 * Возвращает массив ссылок на учебники (параграфы учебников)
	 *
	 * @param homework {*} - домашнее задание
	 * @returns {Array} - массив ссылок на учебники
	 */
	getBooks(homework) {
		const description = _.get(homework, "homework_entry.description", "");
		const fromDescription = this.getUrlsFromDescription(description);
		const books = _.get(homework, "homework_entry.books", null);

		try {
			const fromBooks = _.map(JSON.parse(books), this.$getBookUrl.bind(this));
			const result = _.union(fromDescription, fromBooks);

			return _.uniq(result, "name");
		} catch (e) {
			return fromDescription;
		}
	}

	/**
	 * Возвращает массив ссылок на тесты
	 * @param homework {*} домашнее задание
	 * @returns {Array} - масив ссылок на тесты
	 */
	getTests(homework) {
		return [];
	}

	$getTestUrl(test) {
		const testObject = {
			url: "",
			name: test.name,
			type: "file"
		};

		return testObject;
	}

	$getScriptUrl(script) {
		const {MeschSessions, UserProfiles, $rootScope} = this.services;
		const profileId = _.get(UserProfiles.currentProfile, "id", "");
		const userId = _.get(UserProfiles.currentProfile, "user_id", "");
		const token = MeschSessions.getToken();
		const baseUrl = $rootScope.isProd ? "https://uchebnik.mos.ru" : "https://uchebnik-stable.mos.ru";

		return {
			url: `${baseUrl}/authenticate?authToken=${token}&profileId=${profileId}&userId=${userId}&backurl=${baseUrl}/composer2/lesson/${script.material_id}/view`,
			name: script.name,
			type: "script"
		};
	}

	$getBookUrl(link) {
		const {MeschSessions, UserProfiles, $rootScope} = this.services;
		const profileId = _.get(UserProfiles.currentProfile, "id", "");
		const userId = _.get(UserProfiles.currentProfile, "user_id", "");
		const token = MeschSessions.getToken();
		const baseUrl = $rootScope.isProd ? "https://uchebnik.mos.ru" : "https://uchebnik-stable.mos.ru";

		const linkObject = {
			url: `${baseUrl}/authenticate?authToken=${token}&profileId=${profileId}&userId=${userId}&backurl=${baseUrl}/player2/books/${link.ebook_id}.${link.version}`,
			name: link.name,
			type: "book"
		};

		if (Boolean(link.path)) {
			linkObject.url += `/articles/${link.path}`;
		}

		return linkObject;
	}

	/**
	 * Возвращает продолжительность домашней работы
	 *
	 * @returns {string}
	 */
	getHumanDuration() {
		const noDuration = _.get(this, "homework_entry.no_duration", false);

		if (_.isEmpty(this.homework_entry) || noDuration) {
			return "";
		}

		// продолжительность в минутах
		if (this.homework_entry.duration < 60) {
			return `${this.homework_entry.duration || 0} мин`;
		}

		// продолжительность в часах. "1 ч 39 мин"
		if (this.homework_entry.duration >= 60 && this.homework_entry.duration < 1440) {
			const hours = _.floor(this.homework_entry.duration / 60);
			const minutesInHours = hours * 60;
			const minutes = this.homework_entry.duration - minutesInHours;

			if (minutes > 0) {
				return `${hours} ч ${minutes} мин`;
			}

			return `${hours} ч `;
		}

		// продолжительность в днях. "2 д 4 ч 31 мин"
		if (this.homework_entry.duration >= 1440) {
			const fullDays = _.floor(this.homework_entry.duration / (24 * 60));
			const remainMinutes = this.homework_entry.duration - (fullDays * 24 * 60);
			const fullHours = _.floor(remainMinutes / 60);
			const fullMinutes = remainMinutes - (fullHours * 60);

			let result = "";

			if (fullDays) {
				result += `${fullDays} д `;
			}

			if (fullHours) {
				result += `${fullHours} ч `;
			}

			if (fullMinutes) {
				result += `${fullMinutes} мин `;
			}

			return _.trim(result);
		}

		return "";
	}

	customPUT(homework) {
		const {CookiesStorage} = this.services;
		const headers = {
			[this.tokenHeaderName]: CookiesStorage.get(this.tokenFieldName),
			"Profile-Id": CookiesStorage.get("profileId")
		};

		return homework.customPUT(null, null, null, headers);
	}
}