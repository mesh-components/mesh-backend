import _ from "main/app.mixins";

class MassIncidents {
	/**
	 * @ngInject
	 */
	constructor($q, Argus, ExternalSystems) {
		this.services = {$q, Argus, ExternalSystems};
		this.api = () => Argus.service("/mass_incidents");
	}

	getIncidents(params) {
		const {$q, ExternalSystems} = this.services;

		return $q
			.all({
				systems: ExternalSystems.getAvailableSystems(),
				incidents: this.api().getList(params)
			})
			.then((response) => {
				this.systems = response.systems;

				return this.processIncidents(response.incidents);
			});
	}

	processIncidents(incidents) {
		return _.chain(incidents)
			.map(this.$assignSystems.bind(this))
			.value();
	}

	$assignSystems(incident) {
		incident.systemLogo = _.find(this.systems, {id: incident.externalSystemId}).logo;

		return incident;
	}
}
export {MassIncidents};