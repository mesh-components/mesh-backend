import _ from "main/app.mixins";

class FaqList {
	/**
	 * @ngInject
	 */
	constructor($q, Argus, ExternalSystems) {
		this.services = {$q, Argus, ExternalSystems};
		this.api = () => Argus.service("/faq_list");
	}

	getQuestions(params) {
		const {$q, ExternalSystems} = this.services;

		return $q
			.all({
				systems: ExternalSystems.getAvailableSystems(),
				questions: this.api().getList(params)
			})
			.then((response) => {
				this.systems = response.systems;

				return this.processQuestions(response.questions);
			});
	}

	processQuestions(questions) {
		return _.chain(questions)
			.map(this.$assignSystems.bind(this))
			.value();
	}

	$assignSystems(question) {
		question.systemLogo = _.find(this.systems, {id: question.externalSystemId}).logo;

		return question;
	}
}
export {FaqList};