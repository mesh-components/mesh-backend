class CoreAttachments {

	/**
	 * @ngInject
	 */
	constructor(Core, Upload, CookiesStorage, AUTH_TOKEN_FIELD_NAME, AUTH_TOKEN_HEADER_NAME) {
		this.services = {Core, Upload, CookiesStorage, AUTH_TOKEN_FIELD_NAME, AUTH_TOKEN_HEADER_NAME};
		this.uploadUrl = "/core/api/attachments";
		this.tokenFieldName = AUTH_TOKEN_FIELD_NAME;
		this.tokenHeaderName = AUTH_TOKEN_HEADER_NAME;
	}

	api() {
		const {Core} = this.services;

		return Core.service("/attachments");
	}

	customApi() {
		const {Core} = this.services;

		return Core.all("/attachments");
	}

	/**
	 * Загрузка файла на сервер
	 */
	upload(file) {
		const {Upload, CookiesStorage} = this.services;

		return Upload
			.upload({
				url: this.uploadUrl,
				file,
				headers: {
					Accept: "application/json",
					[this.tokenHeaderName]: CookiesStorage.get(this.tokenFieldName),
					"Profile-Id": CookiesStorage.get("profileId")
				},
				sendFieldsAs: "form"
			});
	}

	removeById(id) {
		return this.customApi().customDELETE(id);
	}

	/**
	 * Возвращает размер файла в читаемом виде
	 */
	getFileSizeString(size) {
		const KB = 1024;
		const MB = KB * 1024;
		const GB = MB * 1024;

		if ((size >= 0) && (size < KB)) {
			return `${size}б`;
		} else if ((size >= KB) && (size < MB)) {
			return `${(size / KB).toFixed(2)}Кб`;
		} else if ((size >= MB) && (size < GB)) {
			return `${(size / MB).toFixed(2)}Мб`;
		}

		return `${(size / GB).toFixed(2)}Гб`;
	}
}

export {CoreAttachments};