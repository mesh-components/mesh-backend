import {MeschInterceptors} from "./wrappers/MeschInterceptors.js";
import {Argus} from "./wrappers/Argus.js";
import {Core} from "./wrappers/Core.js";
import {Jersey} from "./wrappers/Jersey.js";
import {LMSv2} from "./wrappers/LMSv2.js";
import {Reports} from "./wrappers/Reports.js";
import {ACL} from "./wrappers/ACL.js";
import {Survey} from "./wrappers/Survey.js";
import {Chat} from "./wrappers/Chat.js";

import {Attachments} from "./services/Attachments.js";
import {CookiesStorage} from "./services/CookiesStorage.js";
import {Captcha} from "./services/Captcha.js";
import {EducationLevels} from "./services/EducationLevels.js";
import {ClassLevels} from "./services/ClassLevels.js";
import {Counties} from "./services/Counties.js";
import {ClassUnits} from "./services/ClassUnits.js";
import {InformationTypes} from "./services/InformationTypes.js";
import {Schools} from "./services/Schools.js";
import {MeschSessions} from "./services/MeschSessions.js";
import {Subjects} from "./services/Subjects.js";
import {TargetCategories} from "./services/TargetCategories.js";
import {TargetTemplates} from "./services/TargetTemplates.js";
import {TargetGroups} from "./services/TargetGroups.js";
import {Users} from "./services/Users.js";
import {UserProfiles} from "./services/UserProfiles.js";
import {Tickets} from "./services/Tickets.js";
import {Errors} from "./services/Errors.js";
import {TicketTemplate} from "./factories/TicketTemplate.js";
import {LinksFactory} from "./factories/LinksFactory.js";
import {TargetingCategoryFactory} from "./services/TargetingCategoryFactory.js";
import {ExternalSystems} from "./services/ExternalSystems.js";
import {FaqList} from "./services/FaqList.js";
import {MassIncidents} from "./services/MassIncidents.js";
import {NewsFilter} from "./services/NewsFilter.js";
import {NewsCategories} from "./services/NewsCategories.js";
import {News} from "./services/News.js";
import {Links} from "./services/Links.js";
import {Chats} from "./services/Chats.js";
import {ChatMessages} from "./services/ChatMessages.js";
import {CoreProfiles} from "./services/CoreProfiles.js";
import {ChatGroups} from "./services/ChatGroups.js";
import {Permissions} from "./services/Permissions.js";
import {Impersonation} from "./services/Impersonation.js";
import {FAQ} from "./services/FAQ.js";
import {Surveys} from "./services/Surveys.js";
import {SurveysQuestions} from "./services/SurveysQuestions.js";
import {SurveysPassings} from "./services/SurveysPassings.js";

import {AcademicYears} from "./services/AcademicYears.js";
import {AwareJournals} from "./services/AwareJournals.js";
import {StudentGroups} from "./services/StudentGroups.js";
import {ScheduleItems} from "./services/ScheduleItems.js";
import {StudentHomeworks} from "./services/StudentHomeworks.js";
import {Marks} from "./services/Marks.js";
import {StudentProfiles} from "./services/StudentProfiles.js";
import {TeacherProfiles} from "./services/TeacherProfiles.js";
import {ControlForms} from "./services/ControlForms.js";
import {Stationery} from "./services/Stationery.js";
import {StudyBooks} from "./services/StudyBooks.js";
import {StudentBag} from "./services/StudentBag.js";
import {BellsTimetableAssignments} from "./services/BellsTimetableAssignments.js";
import {BellsTimetables} from "./services/BellsTimetables.js";
import {Buildings} from "./services/Buildings.js";
import {Rooms} from "./services/Rooms.js";
import {ECLessons} from "./services/ECLessons.js";
import {ECMarks} from "./services/ECMarks.js";
import {ECControlForms} from "./services/ECControlForms.js";
import {AttendanceNotifications} from "./services/AttendanceNotifications.js";
import {ISPPEvents} from "./services/ISPPEvents.js";
import {CoreClassUnits} from "./services/CoreClassUnits.js";
import {Calendars} from "./services/Calendars.js";
import {CoreAttachments} from "./services/CoreAttachments.js";
import {AELessons} from "./services/AELessons.js";
import {AEMarks} from "./services/AEMarks.js";
import {AEControlForms} from "./services/AEControlForms.js";
import {AEGroups} from "./services/AEGroups.js";
import {AEFakeGroups} from "./services/AEFakeGroups";
import {BigQuery} from "./services/BigQuery.js";
import {ISPPSummary} from "./services/ISPPSummary.js";
import {CoreSubjects} from "./services/CoreSubjects.js";
import {SchoolSettings} from "./services/SchoolSettings.js";
import {ProgressReport} from "./services/ProgressReport.js";
import {StudentHomeworksDiscrepancy} from "./services/StudentHomeworksDiscrepancy.js";
import {ParallelCurricula} from "./services/ParallelCurricula.js";
import {PeriodSchedule} from "./services/PeriodSchedule.js";
import {AclUsers} from "./services/AclUsers.js";
import {CoreUserRelationships} from "./services/CoreUserRelationships.js";
import {CoreSchools} from "./services/CoreSchools.js";
import {RequiresDecisions} from "./services/RequiresDecisions.js";
import {CoreClassLevels} from "./services/CoreClassLevels";
import {TargetMessages} from "./services/TargetMessages";

import {injectConstants} from "mesh-constants";

const meschBackend = angular
    .module("mesch.backend", ["ngCookies", "restangular", "angular-md5"])
    .service("MeschInterceptors", MeschInterceptors)
    .factory("Argus", Argus)
    .factory("Core", Core)
    .factory("Jersey", Jersey)
    .factory("LMSv2", LMSv2)
    .factory("Reports", Reports)
    .factory("ACL", ACL)
    .factory("Survey", Survey)
    .factory("Chat", Chat)

    .service("CookiesStorage", CookiesStorage)
    .service("Attachments", Attachments)
    .service("Captcha", Captcha)
    .service("ClassLevels", ClassLevels)
    .service("ClassUnits", ClassUnits)
    .service("InformationTypes", InformationTypes)
    .service("Schools", Schools)
    .service("MeschSessions", MeschSessions)
    .service("Subjects", Subjects)
    .service("TargetCategories", TargetCategories)
    .service("TargetTemplates", TargetTemplates)
    .service("TargetGroups", TargetGroups)
    .service("Users", Users)
    .service("UserProfiles", UserProfiles)
    .service("Permissions", Permissions)
    .service("Tickets", Tickets)
    .service("Errors", Errors)
    .factory("TicketTemplate", TicketTemplate)
    .factory("LinksFactory", LinksFactory)
    .service("TargetingCategoryFactory", TargetingCategoryFactory)
    .service("EducationLevels", EducationLevels)
    .service("ExternalSystems", ExternalSystems)
    .service("FaqList", FaqList)
    .service("MassIncidents", MassIncidents)
    .service("NewsCategories", NewsCategories)
    .service("NewsFilter", NewsFilter)
    .service("News", News)
    .service("Links", Links)
    .service("AcademicYears", AcademicYears)
    .service("AwareJournals", AwareJournals)
    .service("ScheduleItems", ScheduleItems)
    .service("StudentGroups", StudentGroups)
    .service("StudentHomeworks", StudentHomeworks)
    .service("Marks", Marks)
    .service("CoreSchools", CoreSchools)
    .service("ChatGroups", ChatGroups)
    .service("CoreProfiles", CoreProfiles)
    .service("Chats", Chats)
    .service("ChatMessages", ChatMessages)
    .service("StudentProfiles", StudentProfiles)
    .service("TeacherProfiles", TeacherProfiles)
    .service("ControlForms", ControlForms)
    .service("Stationery", Stationery)
    .service("StudyBooks", StudyBooks)
    .service("StudentBag", StudentBag)
    .service("BellsTimetableAssignments", BellsTimetableAssignments)
    .service("CoreUserRelationships", CoreUserRelationships)
    .service("BellsTimetables", BellsTimetables)
    .service("Buildings", Buildings)
    .service("Rooms", Rooms)
    .service("ECLessons", ECLessons)
    .service("ECMarks", ECMarks)
    .service("ECControlForms", ECControlForms)
    .service("AttendanceNotifications", AttendanceNotifications)
    .service("ISPPEvents", ISPPEvents)
    .service("CoreClassUnits", CoreClassUnits)
    .service("Calendars", Calendars)
    .service("CoreAttachments", CoreAttachments)
    .service("AELessons", AELessons)
    .service("AEMarks", AEMarks)
    .service("AEControlForms", AEControlForms)
    .service("AEGroups", AEGroups)
    .service("AEFakeGroups", AEFakeGroups)
    .service("BigQuery", BigQuery)
    .service("ISPPSummary", ISPPSummary)
    .service("CoreSubjects", CoreSubjects)
    .service("SchoolSettings", SchoolSettings)
    .service("ProgressReport", ProgressReport)
    .service("StudentHomeworksDiscrepancy", StudentHomeworksDiscrepancy)
    .service("ParallelCurricula", ParallelCurricula)
    .service("PeriodSchedule", PeriodSchedule)
    .service("AclUsers", AclUsers)
    .service("Impersonation", Impersonation)
    .service("FAQ", FAQ)
    .service("RequiresDecisions", RequiresDecisions)
    .service("CoreClassLevels", CoreClassLevels)
    .service("TargetMessages", TargetMessages)
    .service("Surveys", Surveys)
    .service("SurveysQuestions", SurveysQuestions)
    .service("SurveysPassings", SurveysPassings)
    .service("ClassLevels", ClassLevels)
    .service("Counties", Counties);

    injectConstants(meschBackend);

export default meschBackend;
