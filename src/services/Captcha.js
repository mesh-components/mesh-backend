class Captcha {

	/**
	 * @ngInject
	 */
	constructor(Argus) {
		this.services = {Argus};
	}

	api() {
		const {Argus} = this.services;

		return Argus.service("/captcha");
	}

	getCaptcha() {
		return this.api()
			.one("")
			.get();
	}

	checkCaptcha(Id, Value) {
		return this.api()
			.post({
				Id,
				Value
			});
	}
}

export {Captcha};