/**
 * уведомления об отсутствии
 *
 * {
 *		id: 123,
 *		date: "2016-01-18",
 *		bell_id: 123,
 *		teacher_profile_id: 123,
 *		parent_profile_id: 123,
 *		created_at: "2016-01-14 10:10:10",
 *		attendance_reason_id: 123,
 *		description: "",
 *		student_profile_id: 123
 * }
 *
 */
export class AttendanceNotifications {
	/**
	 * @ngInject
	 */
	constructor(Core) {
		this.services = {Core};
		this.absenceReasons = [
			{
				id: 1,
				name: "По болезни (со справкой)"
			},
			{
				id: 2,
				name: "Плохое самочувствие"
			},
			{
				id: 3,
				name: "По семейным обстоятельствам"
			},
			{
				id: 4,
				name: "Укажите другую причину"
			}
		];
	}

	api() {
		const {Core} = this.services;

		return Core.service("/attendance_notifications");
	}
}