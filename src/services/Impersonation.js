export class Impersonation {
	/**
	 * @ngInject
	 */
	constructor(ACL) {
		this.services = {ACL};
	}

	api() {
		const {ACL} = this.services;

		return ACL.service("/impersonation");
	}

	async impersonateProfile(profile) {
		if (!profile) {
			return;
		}

		return await this
			.api()
			.post({
				target_profile_id: profile.id
			});
	}
}