import _ from "main/app.mixins";

/**
 * @ngInject
 */
function LinksFactory(Argus, CookiesStorage) {
	return {
		getInstance: () => {
			return Argus
				.withConfig(RestangularConfigurer => {
					const headers = _.clone(RestangularConfigurer.defaultHeaders);

					headers.authToken = CookiesStorage.get("authToken");
					RestangularConfigurer.setDefaultHeaders(headers);
				})
				.service("/useful_links");
		},
		custom: (url) => {
			return Argus
				.withConfig(RestangularConfigurer => {
					const headers = _.clone(RestangularConfigurer.defaultHeaders);

					headers.authToken = CookiesStorage.get("authToken");
					RestangularConfigurer.setDefaultHeaders(headers);
				})
				.all(url);
		},
		getTemplate() {
			const template = {
				categoryId: null, // id категории из /api/useful_links/categories
				publishDate: "", // дата публикации ссылки
				isArchived: false, // флаг архива для ссылок, перенесенных в архив
				title: "", // наименование ссылки
				detail: "", // описание ссылки
				previewImage: null, // id изображения из /api/attachments
				isVip: false, // флаг, определяющий является ли ссылка vip-ссылкой
				contacts: [
					{
						type: "url", // тип контакта (url или phone)
						value: "", // значение контакта (ссылка или номер телефона)
						displayed: true // флаг, определяющий, выбрал ли пользователь этот контакт, как отображаемый в интерфейсе
					},
					{
						type: "phone",
						value: "",
						displayed: false
					}
				],
				targetTemplate: {
					name: null,
					targetCategories: [] // массив с категориями охвата, заданными для новости. Для одной новости категорий может быть несколько,
				}
			};

			return _.clone(template, true);
		}
	};
}

export {LinksFactory};