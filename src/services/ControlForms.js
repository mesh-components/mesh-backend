/**
 * Формы контроля
 */
export class ControlForms {
	/**
	 * @ngInject
	 */
	constructor(Core) {
		this.services = {Core};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/control_forms");
	}
}