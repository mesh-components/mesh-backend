import _ from "main/app.mixins";

class ExternalSystems {
	/**
	 * @ngInject
	 */
	constructor(Argus, $q) {
		this.services = {Argus, $q};
		this.isLoading = true;

		this.availableSystems = [];

		this.ppso = {
			id: 0,
			title: "МЭШ",
			color: "#9bb5c0",
			description: "Московская электронная школа",
			logo: require("images/systems/ppso.svg")
		};
		this.loadAvailableSystems();
	}

	api() {
		const {Argus} = this.services;

		return Argus.service("/external_systems");
	}

	drop() {
		this.availableSystems = [];
	}

	getAvailableSystems() {
		const {$q} = this.services;
		// если список доступных систем не загружается и он пуст, то пробуем загрузить еще раз

		if (!this.isLoading && _.isEmpty(this.availableSystems)) {
			return this.loadAvailableSystems();
		}
		// если список загружается, то ждем когда загрузится
		if (this.isLoading) {
			return $q((r, rj) => {
				const t = setInterval(() => {
					if (!this.isLoading) {
						clearInterval(t);
						r(angular.copy(this.availableSystems));
					}
				}, 200);
			});
		}
		// список не загружается и не пустой
		return $q((r, rj) => {
			r(angular.copy(this.availableSystems));
		});
	}

	loadAvailableSystems() {
		this.isLoading = true;

		return this
			.api()
			.one("available", null)
			.getList()
			.then(data => {
				this.availableSystems = data || [];

				this.isLoading = false;
			});
	}
}

export {ExternalSystems};