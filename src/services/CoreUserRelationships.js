export class CoreUserRelationships {
	/**
	 * @ngInject
	 */
	constructor(Core) {
		this.services = {Core};

		this.types = [
			{
				id: 1,
				name: "Отец"
			},
			{
				id: 2,
				name: "Мать"
			},
			{
				id: 3,
				name: "Законный представитель"
			},
			{
				id: 4,
				name: "опекун"
			},
			{
				id: 5,
				name: "попечитель"
			},
			{
				id: 6,
				name: "представитель органа опеки и попечительства"
			},
			{
				id: 7,
				name: "доверенный представитель"
			},
			{
				id: 8,
				name: "не указан на портале"
			}
		];
	}

	api() {
		const {Core} = this.services;

		return Core.service("/user_relationships");
	}
}