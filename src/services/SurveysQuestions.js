import _ from "main/app.mixins";

class SurveysQuestions {
	/**
	 * @ngInject
	 */
	constructor(Survey, BigQuery, TargetingCategoryFactory, TargetCategories, UrsAlert) {
		this.services = {Survey, BigQuery, TargetingCategoryFactory, TargetCategories, UrsAlert};
		this.targetCategories = TargetCategories.getTargetingCategories() || [];
		this.fullApi = Survey.withConfig((RestangularConfigurer) => RestangularConfigurer.setFullResponse(true));
		this.categoriesUrl = "/surveys/categories";
		this.url = "/surveys/questions";

		this.questionTypes = [
			{title: "С выбором ответа", value: "select"},
			{title: "С открытым ответом", value: "text"}
		];

		this.answerVariantTemplate = {
			title: null,
			previewImage: null,
			attachmentIds: []
		};

		this.questionTemplate = {
			title: null,
			description: null,
			types: ["select"],
			transitions: [],
			variants: [], /* this.getAnswerVariantTemplate() */
			previewImage: null,
			attachmentIds: []
		};
	}

	api() {
		const {Survey} = this.services;

		return Survey.service(this.url);
	}

	customApi(url = this.url) {
		return this.fullApi.all(url);
	}

	async getNewId(type) {
		const {data} = await this.fullApi.all(`booking_id/${type}`).get("");

		return data.id;
	}

	getIdForQuestion() {
		return this.getNewId("question");
	}

	getIdForAnswerVariant() {
		return this.getNewId("question_variant");
	}

	getAll(params={}) {
		const {BigQuery} = this.services;

		return BigQuery.getList(this.api(), params, 100);
	}

	getTypes() {
		return _.clone(this.questionTypes, true);
	}

	async getTemplate() {
		const newQuestion = _.clone(this.questionTemplate, true);

		newQuestion.id = await this.getIdForQuestion();

		return newQuestion;
	}

	async getAnswerVariantTemplate() {
		const newVariant = _.clone(this.answerVariantTemplate, true);
		newVariant.id = await this.getIdForAnswerVariant();

		return newVariant;
	}

	/**
	 * Есть ли у вопроса переход к следующему вопросу
	 * */
	hasTransitionToNext(question) {
		// перехода к следующему вопросу есть если переходы не настроены
		if (!_.get(question.transitions, "length")) {
			return true;
		}

		// перехода к следующему вопросу нет если выбран вариант "в остальных случаях" и не настроены другие переходы
		const nextIsOptional = (_.get(question.transitions, "length") === 1) && _.find(question.transitions, {requires: null});

		if (nextIsOptional) {
			return false;
		}

		// перехода к следующему вопросу нет если заняты все варианты ответов
		const selectVariantsLength = _.chain(question.variants)
			/*.filter({type: "select"})*/
			.get("length", 0)
			.value();

		const variantsIsFull = _.chain(question.transitions)
			.reject({to: null, last: false})
			.map("requires")
			.flatten()
			.map("variantId")
			.uniq()
			.size()
			.isEqual(selectVariantsLength)
			.value();

		return !variantsIsFull;
	}
}

export {SurveysQuestions};