import _ from "main/app.mixins";

export class MeschSessions {

    /**
     * @ngInject
     */
    constructor($location, $state, $q, ACL, AUTH_TOKEN_FIELD_NAME, CookiesStorage, Users, News, NewsFilter, ChatMessages, Permissions, UrsAlert, Surveys, TargetMessages) {
        this.services = {
            $location,
            $state,
            $q,
            ACL,
            CookiesStorage,
            Users,
            News,
            NewsFilter,
            ChatMessages,
            Permissions,
            UrsAlert,
            Surveys,
            TargetMessages
        };
        this.tokenFieldName = AUTH_TOKEN_FIELD_NAME;
    }

    api() {
        const {ACL} = this.services;

        return ACL.service("/sessions");
    }

    init() {
        const {Users, $location, CookiesStorage, ChatMessages, UrsAlert, Surveys, TargetMessages} = this.services;
        const urlToken = $location.search().token;

        // токен в url
        if (urlToken) {
            this.api()
                .post({
                    auth_token: urlToken
                })
                .then((data) => {
                    const profiles = _.get(data, "profiles", []);

                    if (_.isEmpty(profiles)) {
                        UrsAlert.error("Некорректная учетная запись.<br />Попробуйте авторизоваться позднее или воспользуйтесь формой подачи обращения", true);
                        this.drop();

                        return;
                    }


                    CookiesStorage.set(this.tokenFieldName, data[this.tokenFieldName]);
                    CookiesStorage.set("from_pgu", true);
                    Users.setCurrentUser(data);
                    ChatMessages.$checkUnread();
                    TargetMessages.checkNewNotice();
                    this.$redirectByRole(data);
                })
                .catch(() => {
                    this.drop();
                });
        } else {
            // токен в куках
            const token = this.getToken();

            if (Boolean(token)) {
                this.api()
                    .post({
                        auth_token: token
                    })
                    .then((data) => {
                        const profiles = _.get(data, "profiles", []);

                        if (_.isEmpty(profiles)) {
                            UrsAlert.error("Некорректная учетная запись.<br />Попробуйте авторизоваться позднее или воспользуйтесь формой подачи обращения", true);
                            this.drop();

                            return;
                        }

                        ChatMessages.$checkUnread();
                        Surveys.startNotification();
                        TargetMessages.checkNewNotice();

                        return Users.setCurrentUser(data);
                    })
                    .catch(() => {
                        this.drop();
                    });
            }
        }
    }

    getToken() {
        const {CookiesStorage} = this.services;

        return CookiesStorage.get(this.tokenFieldName);
    }

    auth(login, password) {
        const {$q, CookiesStorage, Users, ChatMessages, UrsAlert, Surveys, TargetMessages} = this.services;
        let userData;

        return this
            .api()
            .post({
                login,
                password_plain: password
            })
            .then((data) => {
                const profiles = _.get(data, "profiles", []);

                if (_.isEmpty(profiles)) {
                    UrsAlert.error("Некорректная учетная запись.<br />Попробуйте авторизоваться позднее или воспользуйтесь формой подачи обращения", true);
                    this.drop();

                    return;
                }

                userData = data;

                return $q
                    .all([
                        CookiesStorage.set(this.tokenFieldName, data[this.tokenFieldName]),
                        Users.setCurrentUser(data)
                    ]);
            })
            .then(() => {
                this.$redirectByRole(userData);
                Surveys.startNotification();
                ChatMessages.$checkUnread();
                TargetMessages.checkNewNotice();
            });
    }

    isAuthorized() {
        const {CookiesStorage} = this.services;

        return Boolean(CookiesStorage.get(this.tokenFieldName));
    }

    dropEntities() {
        const {Users, News, NewsFilter, ChatMessages, TargetMessages} = this.services;

        Users.drop();
        News.drop();
        NewsFilter.drop();
        ChatMessages.drop();
        TargetMessages.drop();
    }

    drop() {
        const {CookiesStorage, $state} = this.services;

        return CookiesStorage
            .drop()
            .then(() => {
                this.dropEntities();
            })
            .finally(() => {
                $state.$go("support");
            });
    }

    $redirectByRole(userData) {
        const {Permissions, $state} = this.services;
        let profiles = _.get(userData, "profiles", []);

        const argusProfileIx = _.findIndex(profiles, (profile) => profile.type === "argus" || profile.original_type === "ArgusProfile");

        if (argusProfileIx > 0) {
            profiles = _.move(profiles, argusProfileIx, 0);
        }

        const redirects = [
            {
                rolesAndFeatures: ["parent", "student"],
                toState: "student_journal"
            },
            {
                rolesAndFeatures: ["admin", "school_admin", "urs_admin", "urs_manage_access", "AddAndUpdateRolesBySchools"],
                toState: "admin"
            },
            {
                rolesAndFeatures: ["AddAndUpdateFaq"],
                toState: "support.table",
                params: {type: "draft"}
            },
            {
                rolesAndFeatures: ["AddAndUpdateBypass", "AddAndUpdateEvent"],
                toState: "news.table",
                params: {type: "draft"}
            },
            {
                rolesAndFeatures: ["AddAndUpdateSurvey"],
                toState: "surveys.table",
                params: {type: "draft"}
            },
            {
                rolesAndFeatures: ["AddAndUpdateTargetMessages"],
                toState: "target_messages.table",
                params: {type: "draft"}
            },
            {
                rolesAndFeatures: ["AddAndUpdateUsefulLinks"],
                toState: "links.table",
                params: {type: "draft"}
            }
        ];

        let goNext = true;

        _.forEach(profiles, (profile) => {
            _.forEach(redirects, (redirect) => {
                if (Permissions.hasRolesFeatures(redirect.rolesAndFeatures, profile)) {
                    $state.go(redirect.toState, redirect.params);
                    goNext = false;

                    return false;
                }
            });

            return goNext;
        });

        if (goNext) {
            $state.go("news.feed");
        }
    }
}