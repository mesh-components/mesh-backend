/**
 *
 */
export class CoreClassLevels {
	/**
     * @ngInject
     */
	constructor(Core) {
		this.services = {Core};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/class_levels");
	}
}