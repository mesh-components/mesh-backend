import _ from "main/app.mixins";
import moment from "main/moment-local";
import archiveIcon from "images/news/archive.svg";
import allIcon from "images/news/all_news.svg";

export class News {
    /**
     * @ngInject
     */
    constructor($q, Argus, TargetCategories, InformationTypes, ExternalSystems, NewsCategories, DEFAULT_DATE_FORMAT,
                Attachments, $window, $timeout, Schools, ClassLevels, ClassUnits, Subjects, UTC_FORMAT,
                TargetingCategoryFactory, $anchorScroll) {
        this.services = {
            $q, $window, $timeout,
            Argus, TargetCategories,
            InformationTypes, ExternalSystems,
            NewsCategories, Attachments,
            Schools, ClassLevels, ClassUnits, Subjects,
            UTC_FORMAT,
            TargetingCategoryFactory,
            $anchorScroll
        };
        this.DEFAULT_DATE_FORMAT = DEFAULT_DATE_FORMAT;
        this.informationTypes = [];
        this.scrollTopOffset = 0;
        this.pagesTotal = 1;
        this.perPage = 11;
        this.detailMode = {
            news: null
        };
        this.genderCategories = {
            male: "М",
            female: "Ж"
        };
        this.catDiffs = {
            all: {
                title: "Все новости"
            },
            staff: {
                title: "Новости для сотрудников сферы образования"
            },
            parent: {
                title: "Новости для родителей"
            },
            student: {
                title: "Новости для учащихся"
            },
            archive: {
                title: "Архив новостей"
            }
        };
        this.commonIcons = {
            archive: archiveIcon,
            all: allIcon
        };
        this.apiUrl = "/news";
        this.commonData = {};
        const self = this;
        // расширяем модель
        Argus.extendModel(self.apiUrl, (model) => {
            model.getFormattedCreateDate = self.$getFormattedCreateDate;
            model.getFormattedEditedDate = self.$getFormattedEditedDate;
            model.getFormattedPublishDate = self.$getFormattedPublishDate;
            model.getFormattedEndPublishDate = self.$getFormattedEndPublishDate;

            return model;
        });
        this.fullApi = Argus.withConfig((RestangularConfigurer) => RestangularConfigurer.setFullResponse(true));
        this.targetCategories = this.getTargetingCategories();
        this.$loadInformationTypes();
        this.loadExternalSystemsPromise = this.$loadExternalSystemsForNews();
        this.loadCategoriesPromise = this.$loadCategoriesForNews();
        this.PUBLISH_DATE_FORMAT = "YYYY-MM-DDT00:00:00+0000";

        this.NEWS_TEMPLATE = {
            isAvailableForUnauthorized: true, // флаг определяющий доступность новости для неавторизованного пользователя
            informationType: "news", // Тип информации: Новости(news), События(announcement), Обходное решение(bypass)
            externalSystemIds: [], // массив id внешних систем из списка /api/external_systems/available
            categoryId: null, // id категории новости из /api/news/categories
            publishDate: "", // дата публикации новости DD.MM.YYYY
            publishPeriod: 30, // период публикации. количество дней публикации начиная с даты публикации. По-умолчанию 30 дней.
            title: "", // заголовок новости
            detail: "", // текст новости
            previewImage: null, // id изображения, которое превью для новости
            images: [], // id прикрепленных к новости изображений // файлы из /api/attachments
            isTop: false, // флаг, определяющий является ли новость топовой
            weight: 3, // уровень важности (вес новости)
            targetTemplate: {
                name: null,
                counties: [],
                // массив с категориями охвата, заданными для новости. Для одной новости категорий может быть несколько,
                targetCategories: _.map(this.targetCategories, (category) => TargetingCategoryFactory.get({type: category.params.type}))
            }
        };

        this.weightRange = [
            {
                value: 1,
                title: "Критичный"
            },
            {
                value: 2,
                title: "Высокий"
            },
            {
                value: 3,
                title: "Базовый"
            }
        ];

        this.sources = [
            {
                id: 1,
                shortTitle: "образовательных организаций",
                title: "Уровень новости 'Образовательные организации'"
            },
            {
                id: 2,
                shortTitle: "городские",
                title: "Уровень новости 'городские'"
            }
        ];
    }

    /**
     * Возвращает дату создания в заданном формате
     *
     * @param dateFormat {String} формат даты
     *
     * @returns {*}
     */
    $getFormattedCreateDate(dateFormat = "DD/MM/YYYY") {
        const date = _.get(this, "createdDate", null);

        if (!date) {
            return "";
        }

        return moment(date).local().format(dateFormat);
    }

    /**
     * Возвращает дату последнего редактирования в заданном формате
     *
     * @param dateFormat {String} формат даты
     *
     * @returns {*}
     */
    $getFormattedEditedDate(dateFormat = "DD/MM/YYYY") {
        const date = _.get(this, "editedDate", null);

        if (!date) {
            return "";
        }

        return moment(date).local().format(dateFormat);
    }

    /**
     * Возвращает дату публикации в заданном формате
     *
     * @param dateFormat {String} формат даты
     *
     * @returns {*}
     */
    $getFormattedPublishDate(dateFormat = "DD/MM/YYYY") {
        const PUBLISH_DATE_FORMAT = "YYYY-MM-DDT00:00:00";
        const date = _.get(this, "publishDate", null);

        if (!date) {
            return "";
        }

        return moment(date, PUBLISH_DATE_FORMAT).format(dateFormat);
    }

    /**
     * Возвращает дату конца публикации в заданном формате
     *
     * @param dateFormat {String} формат даты
     *
     * @returns {*}
     */
    $getFormattedEndPublishDate(dateFormat = "DD/MM/YYYY") {
        const PUBLISH_DATE_FORMAT = "YYYY-MM-DDT00:00:00";
        const date = _.get(this, "publishDate", null);
        const period = _.get(this, "publishPeriod", null);

        if (!date || !period) {
            return "";
        }

        return moment(date, PUBLISH_DATE_FORMAT).add(period, "days").format(dateFormat);
    }

    api() {
        const {Argus} = this.services;

        return Argus.service(this.apiUrl);
    }

    drop() {

    }

    customApi(url = "/news") {
        return this.fullApi.all(url);
    }

    $checkInitialDataLoaded() {
        const {$q} = this.services;

        return $q.all([
            this.loadExternalSystemsPromise,
            this.loadCategoriesPromise
        ]);
    }

    /**
     * Загрузка списка новостей
     */
    getPage(num = 1, params) {
        const completeParams = _.assign({page: num, information_types: "news"}, params);

        return this.fullApi
            .all("/news")
            .getList(completeParams)
            .then((response) => {
                const result = {
                    items: response.data,
                    pageNum: _.int(response.headers("X-Pagination-Page")) || 1,
                    pagesTotal: _.int(response.headers("X-Pagination-Total-Pages")) || 0,
                    itemsTotal: _.int(response.headers("X-Pagination-Total-Items")) || 0
                };

                return this.$checkInitialDataLoaded().then(() => {
                    _.forEach(result.items, (item) => {
                        this.$processNews(item);
                    });

                    return this.$getPreviewImages(result);
                });
            });
    }

    getNewsById(id) {
        return this.api()
            .one(id)
            .get()
            .then((resp) => {
                return this.$checkInitialDataLoaded().then(() => {
                    this.$processNews(resp);

                    return this.$getPreviewImages({items: [resp]});
                });
            })
            .then((resp) => resp.items[0]);
    }

    /**
     * Загрузка числа аудитории, охватываемой настройками таргетинга
     */
    getTargetCount(params) {
        return this
            .customApi("target_templates/target_count")
            .post(params);
    }

    /**
     * Возвращает шаблон новости
     */
    getTemplate() {
        return _.clone(this.NEWS_TEMPLATE, true);
    }

    /**
     * Возвращает категории таргетинга новостей
     */
    getTargetingCategories() {
        const {TargetCategories} = this.services;

        return TargetCategories.getTargetingCategories(this.catDiffs);
    }

    nextNews(newsId) {
        return this.$switchNews("getnext", newsId);
    }

    previousNews(newsId) {
        return this.$switchNews("getprevious", newsId);
    }

    /**
     * Загрузка новостных событий
     */
    getAnnouncements(params) {
        params.information_types = "announcement";

        return this.getPage(params, 1)
            .then((resp) => {
                return resp.items;
            });
    }

    /**
     * Открыть детальное отображение новости
     * */
    openNews(news) {
        const {$window} = this.services;

        this.detailMode.news = news;
        this.scrollTopOffset = $window.scrollY;
    }

    /**
     * Возвращение вертикального скролла при переходе от детального отображения к списку новостей
     */
    backScrollTop() {
        const {$window, $timeout} = this.services;

        if (this.scrollTopOffset) {
            $timeout(() => {
                $window.scrollBy(0, this.scrollTopOffset);
                this.scrollTopOffset = 0;
            });
        }
    }

    /**
     * Возвращает статус новости: черновик, опубликованная, архив, новая
     */
    getNewsState(news) {
        if (!Boolean(news.id)) {
            return "new";
        }
        // если не задана дата публикации, то новость считается черновиком
        if (!Boolean(news.publishDate)) {
            return "draft";
        }
        // если у новости выставлен флаг архивности, то новость считается архивной
        if (news.isArchived) {
            return "archive";
        }

        const publishPeriod = _.int(news.publishPeriod) || 1;
        const publishDateStart = moment(news.publishDate).startOf("day");
        const publishDateEnd = moment(news.publishDate).add(publishPeriod, "days");
        const today = moment().startOf("day");

        // если период публикации закончился, то новость считается архивной
        if (publishDateEnd.isBefore(today)) {
            return "archive";
        }

        // если дата публикации новости наступила, но не закончился период публикации,
        // то новость считается опубликованной
        if (publishDateStart.isBefore(today) || publishDateStart.isSame(today)) {
            return "published";
        }

        // если дата публикации не наступила, то новость считается черновиком

        return "draft";
    }

    $loadInformationTypes() {
        const {InformationTypes} = this.services;

        this.informationTypes = InformationTypes.getInformationTypes();
    }

    $loadExternalSystemsForNews() {
        const {ExternalSystems} = this.services;

        return ExternalSystems
            .getAvailableSystems()
            .then((systems) => {
                this.systems = systems || [];
                this.systems.push(_.clone(ExternalSystems.ppso));

                return this.systems;
            });
    }

    $loadCategoriesForNews() {
        const {NewsCategories} = this.services;

        return NewsCategories
            .getCategories()
            .then((categories) => {
                this.categories = categories || [];

                return this.categories;
            });
    }

    $processNews(news) {
        this.$setSystemLogo(news);
        this.$assignCategory(news);
        this.$assignTargetCategories(news);
        this.$assignInformationTypes(news);

        news.displayedDate = moment(_.get(news, "editedDate")).local().format(" DD MMMM");
    }

    $assignCategory(news) {
        news.category = _.find(this.categories, {id: news.categoryId});
    }

    $setSystemLogo(news) {
        const system = _.find(this.systems, {id: news.externalSystemId});
        news.systemLogo = _.get(system, "logo");
    }

    $assignTargetCategories(news) {
        const newsTCategories = _.get(news, "targetTemplate.targetCategories", []) || [];

        if (newsTCategories.length === 1) {
            news.targetCategory = this.targetCategories[newsTCategories[0].type];
        }

        _.forEach(newsTCategories, (category) => {
            _.assign(category, this.targetCategories[category.type]);

            category.gender = _.map(category.gender, (gender) => {
                return {
                    type: gender,
                    title: this.genderCategories[gender]
                };
            });
        });
    }

    $assignInformationTypes(news) {
        news.informationTypeData = _.find(this.informationTypes, {type: news.informationType});
    }

    /**
     * Загрузка изображений для новости
     */
    $getPreviewImages(newsObj) {
        const {Attachments} = this.services;

        const attachmentsIds = _.chain(newsObj.items)
            .map((item) => item.previewImage || null)
            .compact()
            .uniq()
            .value();

        if (attachmentsIds.length === 0) {
            return newsObj;
        }

        return Attachments
            .getListByIds(attachmentsIds)
            .then((files) => {
                // мапим изображения к новостям
                newsObj.items = _.map(newsObj.items, (item) => {
                    if (item.previewImage) {
                        const preview = _.find(files, {id: item.previewImage});

                        item.previewImageSrc = _.get(preview, "url", "");
                    }

                    return item;
                });

                return newsObj;
            });
    }

    $switchNews(direction, id) {
        const offset_news_id = id || _.get(this.detailMode, "news.id");

        return this
            .customApi("/news")
            .customGET(direction, {id: offset_news_id})
            .then((newsId) => this.getNewsById(newsId));
    }

    /**
     * Прокручивает окно до обязательного к заполнению поля.
     * @param form
     */
    scrollToError(form) {
        const {$anchorScroll} = this.services;

        const elem = _.get(form, "$error.required[0].$name");
        if (elem) {
            $anchorScroll.yOffset = document.documentElement.clientHeight / 2;
            $anchorScroll(elem);
        }
    }
}