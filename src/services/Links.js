import _ from "main/app.mixins";
import LINKS_EMPTY_URL_LOGO from "images/icons/forward.svg";
import LINKS_EMPTY_PHONE_LOGO from "images/icons/i-phone.svg";

export class Links {
	/**
	 * @ngInject
	 */
	constructor($q, Argus, Attachments, BigQuery, TargetingCategoryFactory) {
		this.services = {$q, Argus, Attachments, BigQuery, TargetingCategoryFactory};
		this.fullApi = Argus.withConfig((RestangularConfigurer) => RestangularConfigurer.setFullResponse(true));
		this.url = "/useful_links";
		this.categoriesUrl = "/useful_links/categories";
	}

	api() {
		const {Argus} = this.services;

		return Argus.service(this.url);
	}

	customApi(url = this.url) {
		return this.fullApi.all(url);
	}

	/**
	 * Шаблон ссылки
	 */
	getTemplate() {
		const {TargetingCategoryFactory} = this.services;
		const linkTemplate = {
			categoryId: null, // id категории из /api/useful_links/categories
			publishDate: "", // дата публикации ссылки
			isArchived: false, // флаг архива для ссылок, перенесенных в архив
			title: "", // наименование ссылки
			detail: "", // описание ссылки
			previewImage: null, // id изображения из /api/attachments
			isVip: false, // флаг, определяющий является ли ссылка vip-ссылкой
			contacts: [
				{
					type: "url", // тип контакта (url или phone)
					value: "", // значение контакта (ссылка или номер телефона)
					displayed: true // флаг, определяющий, выбран ли этот контакт, как отображаемый в интерфейсе
				}
			],
			targetTemplate: {
				name: null,
				targetCategories: [
					TargetingCategoryFactory.get({type: "staff"}),
					TargetingCategoryFactory.get({type: "parent"}),
					TargetingCategoryFactory.get({type: "student"})
				]
			}
		};

		return _.clone(linkTemplate, true);
	}


	/**
	 * Загрузка всех ссылок в соответствии с параметрами запроса. В ссылки будет добавлено изображение логотипа.
	 *
	 * @param params {*} параметры запроса
	 *
	 * @returns {Promise.<Array>}
	 */
	async getAll(params = {}) {
		const {BigQuery} = this.services;

		try {
			const items = await BigQuery.getList(this.api(), params, 50);
			const images = await this.getImagesForLinks(items);

			return this.$mapImagesToLinks(items, images);
		} catch (err) {
			return [];
		}
	}

	/**
	 * Загрузка страницы ссылок в соответствии с параметрами запроса. В ссылки будет добавлено изображение логотипа.
	 *
	 * @param page {Number} номер страницы
	 * @param perPage {Number} количество ссылок на страницу
	 * @param params {*} параметры запроса
	 */
	async getPage(page = 1, perPage = 10, params = {}) {
		const requestParams = _.assign({}, params, {page, per_page: perPage});

		try {
			const response = await this
				.customApi()
				.getList(requestParams);

			const items = _.get(response, "data", []);
			const images = await this.getImagesForLinks(items);

			const result = {
				items: this.$mapImagesToLinks(items, images),
				itemsTotal: _.int(response.headers("X-Pagination-Total-Items")) || (params.per_page * result.pagesTotal),
				pageNum: _.int(response.headers("X-Pagination-Page")) || 1,
				pagesTotal: _.int(response.headers("X-Pagination-Total-Pages")) || 1
			};

			return result;
		} catch (err) {
			return [];
		}
	}

	/**
	 * Загрузка ссылки по ее id
	 *
	 * @param id {Number} id ссылки
	 * @param params {*} дополнительные параметры запроса
	 *
	 * @returns {Promise.<*>}
	 */
	async getById(id, params={}) {
		const link = await this.api().one(id).get(params);
		const images = await this.getImagesForLinks([link]);

		if (link.previewImage) {
			link.logo = _.find(images, {id: link.previewImage});
		}

		if (!link.logo) {
			const isUrlContact = _.find(link.contacts, {type: "url"});

			link.logo = {
				url: Boolean(isUrlContact) ? LINKS_EMPTY_URL_LOGO : LINKS_EMPTY_PHONE_LOGO
			};
		}

		return link;
	}

	/**
	 * Возвращает массив ссылок с изображениями внутри
	 *
	 * @param items {Array} массив ссылок /argus/api/useful_links
	 * @param images {Array} массив изображений /argus/api/attachments
	 *
	 * @returns {Array}
	 */
	$mapImagesToLinks(items, images) {
		return _.map(items, (item) => {
			item.logo = _.find(images, {id: item.previewImage});

			if (!item.logo) {
				const isUrlContact = _.find(item.contacts, {type: "url"});

				item.logo = {
					url: Boolean(isUrlContact) ? LINKS_EMPTY_URL_LOGO : LINKS_EMPTY_PHONE_LOGO
				};
			}

			return item;
		});
	}

	/**
	 * Возвращает статус ссылки: черновик, опубликованная
	 */
	getLinkState(link) {
		if (!Boolean(link.id)) {
			return "new";
		}
		// если не задана дата публикации, то ссылка считается черновиком
		if (!Boolean(link.publishDate)) {
			return "draft";
		}

		const publishDateStart = moment(link.publishDate).startOf("day");
		const today = moment().startOf("day");

		if (publishDateStart.isBefore(today) || publishDateStart.isSame(today)) {
			return "published";
		}

		// если дата публикации не наступила, то ссылка считается черновиком

		return "draft";
	}

	/**
	 * Загрузка изображений для ссылок
	 *
	 * @param items {Array} массив ссылок, для которых нужно загрузить изображения. /argus/api/useful_links
	 *
	 * @returns {Promise.<*>}
	 */
	async getImagesForLinks(items) {
		const {Attachments} = this.services;

		if (_.isEmpty(items)) {
			return [];
		}

		// собираем изображения для логотипов
		const logoIds = _.chain(items)
			.map("previewImage")
			.uniq()
			.compact()
			.value();

		if (_.isEmpty(logoIds)) {
			return [];
		}

		return await Attachments.getListByIds(logoIds);
	}

	/**
	 * Загрузка категорий полезных ссылок
	 *
	 * @returns {Promise.<*>}
	 */
	async getCategories() {
		return await this
			.customApi(this.categoriesUrl)
			.getList()
			.then((response) => response.data)
			.catch(() => []);
	}
}