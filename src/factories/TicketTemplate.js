import _ from "main/app.mixins";

/**
 * @ngInject
 */
function TicketTemplate($q, MeschSessions, Users, UserProfiles) {
	const TEMPLATE = {
		externalSystemId: null, // id внешней системы из /external_systems/available
		categoryId: null, // id категории из /external_systems/categories
		description: "", // текст обращения, который вводит пользователь
		attachments: [], // массив id прикрепленных файлов
		firstName: "", // имя пользователя (обязательно, если userId==null)
		lastName: "", // фамилия пользователя (обязательно, если userId==null)
		middleName: "", // отчество пользователя (обязательно, если userId==null)
        organizationName: "",
		organizationId: null, // Id организации пользователя из /api/schools
		problemType: "", // название категории, оправляемое в униформу
		email: "", // email пользователя
		login: "", // логин пользователя
		phone: "", // телефон пользователя
		solutions: [], // массив с решениями по тикету
		status: "new" // текущий статус (определяется по значению полей самого обращения).
		// возможные значения статуса:
		// 		new: 	"Отправлено"
		// 		signed: "Зарегистрировано"
		// 		inProgress: "В работе"
		// 		solved: "Решено"
		// 		closed: "Закрыто"
	};

	return {
		get
	};

	function get() {
		const template = _.clone(TEMPLATE);

		if (!MeschSessions.isAuthorized()) {
			return $q((resolve) => {
				resolve(template);
			});
		}

		return $q
			.all({
				user: Users.getCurrentUser(),
				profile: UserProfiles.getCurrentProfile()
			})
			.then((response) => {
				template.firstName = _.get(response.user, "first_name", null);
				template.lastName = _.get(response.user, "last_name", null);
				template.middleName = _.get(response.user, "middle_name", null);
				template.email = _.get(response.user, "email", null);
				template.phone = _.get(response.user, "phone_number", null);
				template.organizationId = _.get(response.profile, "school_id", null);

				return template;
			});
	}
}

export {TicketTemplate};