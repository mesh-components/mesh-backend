import _ from "main/app.mixins";

const INFORMATION_TYPES = [
	{
		type: "news",
		title: "Новости"
	},
	{
		type: "announcement",
		title: "События"
	},
	{
		type: "mass_incident",
		title: "Массовые инциденты"
	},
	{
		type: "useful_link",
		title: "Полезные ссылки"
	},
	{
		type: "interview",
		title: "Опросы"
	},
	{
		type: "bypass",
		title: "Обходные решения"
	},
	{
		type: "faq",
		title: "Вопрос-ответ"
	}
];

class InformationTypes {
	constructor() {
	}

	getInformationTypes() {
		return _.clone(INFORMATION_TYPES);
	}
}

export {InformationTypes};