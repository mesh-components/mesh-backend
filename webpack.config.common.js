const _ = require("lodash");
/*const componentsLoader = require("./app/main/components.loader");
const hash = componentsLoader.hash;*/

const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const ngAnnotatePlugin = require("ng-annotate-webpack-plugin");
const Bump = require("bump-webpack-plugin");
const webpack = require("webpack");

const libraryAliases = {
	angular: "angular",
	"angular-animate": "angular-animate",
	ngCookies: "angular-cookies",
	ngRoute: "angular-route",
	ngFileUpload: "ng-file-upload",
	"angular-md5": "angular-md5",
	restangular: "restangular",
	ngWYSIWYG: "ng-wysiwyg"
};

const dirAliases = {
	main: __dirname + "/src/main",
/*	components: __dirname + "/app/components",*/
	npm: __dirname + "/node_modules",
	images: __dirname + "/src/images",
	fonts: __dirname + "/fonts"
};

const filesToCopy = [
	{
		from: "./components/common/styles/wysiwyg",
		to: "wysiwyg-css"
	}
];

module.exports = {
	entry: _.assign({
		["build"]: "./src/index.js",
		/*["app"  + (hash ? `.${hash}` : "")]: "./app/main/app.js",*/
		vendor: [
            "ngCookies", "restangular", "angular-md5"
/*			"angular", "angular-animate", "angular-sanitize", "angular-mocks", "chart.js",
			...componentsLoader.getBaseComponents().webpack*/
		]
	}),
	output: {
		path: "./build",
		filename: `[name].js`
	},
	context: __dirname,
	resolve: {
		root: __dirname,
		alias: _.assign({}, libraryAliases, dirAliases),
		extensions: ["", ".js", ".html", ".json"]
	},
	externals: {
		/*webpack_hash: `"${hash}"`,*/
		NODE_ENV: `'${process.env.NODE_ENV}'` // dev | test | production
	},
	devtool: "module-source-map",
	devServer: {
		port: 8090,
		contentBase: "./build",
		proxy: {
			"**/api/**": {
				changeOrigin: true,
				secure: false,
				toProxy: true
			}
		},
		historyApiFallback: {
			index: "/"
		}
	},
	plugins: _.union([
        new ngAnnotatePlugin({add: true}),
		new CleanWebpackPlugin(["build"]),
/*		new HtmlWebpackPlugin({
			template: "./index.template.html",
			inject: "body"
		}),*/
		new CopyWebpackPlugin(filesToCopy),
		new ExtractTextPlugin("[name].css", {allChunks: true}),
        new Bump(["package.json"])
	]),
	module: {
		loaders: [
			{
				test: /\.js$/,
				loader: "babel",
				exclude: /(\.test.js$|node_modules)/
			},
			{
				test: /[\/]angular\.js$/,
				loader: "exports?angular"
			},
			{
				test: /\.css$/,
				exclude: /\.local\.css$/,
				loader: ExtractTextPlugin.extract("style/url!file", "css?sourceMap")
			},
			{
				test: /\.local\.css$/,
				loader: ExtractTextPlugin.extract(
					"style/url!file",
					"css?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]"
				)
			},
			{
				test: /\.local\.scss$/,
				loader: ExtractTextPlugin.extract(
					"style/url!file",
					"!css?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!sass?sourceMap"
				)
			},
			{
				test: /\.scss$/,
				exclude: /\.local\.scss$/,
				loader: ExtractTextPlugin.extract(
					"style/url!file",
					"!css!resolve-url!sass?sourceMap"
				)
			},
			{
				test: /\.html$/,
				loader: "html"
			},
			{
				test: /\.(png|jpg|gif|svg)$/,
				loader: "url?limit=10000" // embed images. max 10Kb
			},
			{
				test: /\.(eot|ttf|woff|woff2)$/,
				loader: "url?limit=500000"
			}
		]
	},
	htmlLoader: {
		root: true
	}
};