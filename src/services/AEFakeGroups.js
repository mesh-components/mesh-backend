export class AEFakeGroups {
    /**
     * @ngInject
     */
    constructor(Core, BigQuery) {
        this.services = {Core, BigQuery};
    }

    api() {
        const {Core} = this.services;

        return Core.service("/ae_fake_groups");
    }

    getAll(params={}) {
        const {BigQuery} = this.services;

        return BigQuery.getList(this.api(), params, 20);
    }
}