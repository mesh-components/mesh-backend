class ClassUnits {

	/**
	 * @ngInject
	 */
	constructor(Argus, BigQuery) {
		this.services = {Argus, BigQuery};
	}

	api() {
		const {Argus} = this.services;

		return Argus.service("/school_dictionaries/class_units");
	}

	getAll(params={}) {
		const {BigQuery} = this.services;

		return BigQuery.getList(this.api(), params, 200);
	}
}

export {ClassUnits};