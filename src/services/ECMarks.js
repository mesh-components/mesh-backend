/**
 * оценки внеурочной деятельности
 */
export class ECMarks {
	/**
	 * @ngInject
	 */
	constructor(Core, BigQuery) {
		this.services = {Core, BigQuery};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/ec_marks");
	}

	/**
	 * Загрузка оценок с формами контроля
	 */
	loadMarks(params) {
		const {BigQuery} = this.services;

		return BigQuery.getList(this.api(), params, 50);
	}
}