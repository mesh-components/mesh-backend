import _ from "main/app.mixins";

class Attachments {
	/**
	 * @ngInject
	 */
	constructor(Argus, BigQuery, Upload, AUTH_TOKEN_HEADER_NAME, AUTH_TOKEN_FIELD_NAME, CookiesStorage) {
		this.services = {Argus, BigQuery, Upload, CookiesStorage};
		this.tokenFieldName = AUTH_TOKEN_FIELD_NAME;
		this.tokenHeaderName = AUTH_TOKEN_HEADER_NAME;
		this.uploadUrl = "/argus/api/attachments";
	}

	api() {
		const {Argus} = this.services;

		return Argus.service("/attachments");
	}

	customApi() {
		const {Argus} = this.services;

		return Argus.all("/attachments");
	}

	getFile(id) {
		return this.api()
			.one(id)
			.get();
	}

	/**
	 * Возвращает размер файла в читаемом виде
	 */
	getFileSizeString(size) {
		const KB = 1024;
		const MB = KB * 1024;
		const GB = MB * 1024;

		if ((size >= 0) && (size < KB)) {
			return `${size}б`;
		} else if ((size >= KB) && (size < MB)) {
			return `${(size / KB).toFixed(2)}Кб`;
		} else if ((size >= MB) && (size < GB)) {
			return `${(size / MB).toFixed(2)}Мб`;
		}

		return `${(size / GB).toFixed(2)}Гб`;
	}

	/**
	 * Загрузка файла на сервер
	 */
	upload(file) {
		const {Upload, CookiesStorage} = this.services;

		return Upload
			.upload({
				url: this.uploadUrl,
				file,
				headers: {
					Accept: "application/json",
					[this.tokenHeaderName]: CookiesStorage.get(this.tokenFieldName),
					"Profile-Id": CookiesStorage.get("profileId")
				},
				sendFieldsAs: "form"
			});
	}

	/**
	 * Загрузка аттачей по id
	 *
	 * @param ids {Array} массив id аттачей
	 *
	 * @returns {*}
	 */
	getListByIds(ids) {
		const {BigQuery} = this.services;


		return BigQuery.getListByChunkedParams(
			this.api(),
			{
				ids: _.uniq(ids).join(",")
			},
			"ids",
			5
		);
	}

	removeById(id) {
		return this.customApi().customDELETE(id);
	}
}

export {Attachments};