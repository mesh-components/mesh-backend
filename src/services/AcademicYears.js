import _ from "main/app.mixins";

/**
 * учебные года
 */
export class AcademicYears {
	/**
	 * @ngInject
	 */
	constructor(Core, $q, $interval, Calendars, CookiesStorage, $injector, Utils) {
		this.services = {Core, $q, $interval, Calendars, CookiesStorage, $injector, Utils};
		this.current = null;
		this.holidays = [];
		this.items = [];
		this.isLoading = false;

		this.pilot_schools = [
			1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 15,
			17, 18, 19, 23, 24, 25, 26, 27, 28,
			29, 50, 52, 53, 54, 55, 56, 57, 58,
			59, 60, 61, 62, 63, 64, 66, 85, 96,
			99, 102, 116, 117, 188, 197, 266, 286,
			288, 310, 375, 382, 383, 385, 386, 390,
			393, 394, 456, 504, 531, 539, 541, 545,
			550, 553, 556, 572, 573, 575, 579, 580,
			581, 590, 592, 595, 596, 597, 599, 600,
			603, 604, 607, 609, 612, 614, 618, 624,
			626, 635, 636, 652, 662, 671, 675, 680,
			685, 689, 690, 696, 698, 700, 701, 707,
			708, 709, 710, 720, 722, 730, 732, 740,
			757, 760, 765, 774, 777, 780, 782, 790,
			791, 794, 811, 814, 817, 824, 843, 846,
			857, 860, 870, 882, 892, 905, 909, 921,
			943, 971, 979, 985, 992, 1004, 1011, 1014,
			1036, 1037, 1041, 1045, 1049, 1052, 1053,
			1057, 1067, 1071, 1074, 1076, 1077, 1084,
			1089, 1095, 1098, 1103, 1104, 1111, 1116,
			1118, 1124, 1131, 1135, 1136, 1137, 1139,
			1140, 1141, 1142
		];
	}

	api() {
		const {Core} = this.services;

		return Core.service("/academic_years");
	}

	loadList() {
		if (!_.isEmpty(this.items)) {
			return;
		}

		this.isLoading = true;

		const {CookiesStorage, $q, $injector} = this.services;

		return $q.all({
			years: this.api().getList(),
			profile: $injector.get("UserProfiles").getCurrentProfile()
		})
			.then((data) => {
				// Удаляем 14-15 и 15-16 год, потому что
				// апишки изменились.
				this.items = _.reject(data.years, (year) => year.id === 1 || year.id === 3);

				const currentYear = _.find(this.items, {current_year: true}) || null;
				const yearId = _.get(currentYear, "id", null);
				// выбираем текущий учебный год
				this.current = currentYear;

				if (yearId) {
					CookiesStorage.set("aid", yearId);
				}

				if (_.intersects(data.profile.roles, ["student", "parent"])) {
					return this.$loadHolidays(); // загружаем праздничные дни
				}

				this.isLoading = false;
			});
	}

	/**
	 * Удалить лишние года по айдишникам школ
	 * @param schoolIds
	 */
	clearItems(schoolIds) {
		// 16-17 год доступен не для всех шкшол
		if (!_.intersects(this.pilot_schools, schoolIds)) {
			this.items = _.reject(this.items, {id: 4});
		}
	}

	/**
	 * Загрузка праздников для учебных годов
	 *
	 * @returns {*}
	 */
	$loadHolidays() {
		const {$q, Calendars} = this.services;
		const requests = [];

		// загружаем праздничные дни
		_.forEach(this.items, (year) => {
			const calendarId = _.get(year, "calendar_id", null);

			if (calendarId) {
				requests.push(Calendars.getYearCalendar(calendarId));
			}
		});

		// мапим праздничные дни на учебные года
		return $q
			.all(requests)
			.then((response) => {
				const allHollidays = _.chain(response)
					.flatten()
					.filter({is_holiday: true})
					.value();

				_.forEach(this.items, (year) => {
					year.holidays = _.filter(allHollidays, {calendar_id: year.calendar_id});
				});

				this.isLoading = false;
			});
	}


	getSelected() {
		const {Utils} = this.services;

		return Utils.waitResource(this, "current", this.loadList.bind(this));
	}


	getList() {
		const {Utils} = this.services;

		return Utils.waitResource(this, "items", this.loadList.bind(this));
	}

	$getCurrent() {
		const {$q, $interval} = this.services;

		return $q((resolve) => {
			if (_.isNull(this.current)) {
				const t = $interval(() => {
					if (!_.isEmpty(this.current)) {
						$interval.cancel(t);
						resolve(this.current);
					}
				}, 200);
			} else {
				resolve(this.current);
			}
		});
	}
}