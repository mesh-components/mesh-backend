/**
 * привязки к режимам пребывания
 */
export class BellsTimetableAssignments {
	/**
	 * @ngInject
	 */
	constructor(Core) {
		this.services = {Core};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/bells_timetable_assignments");
	}
}