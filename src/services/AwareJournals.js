/**
 * ознакомление с дневником
 */
export class AwareJournals {

	/**
	 * @ngInject
	 */
	constructor(LMSv2) {
		this.services = {LMSv2};
	}

	api() {
		const {LMSv2} = this.services;

		return LMSv2.service("/aware_journals");
	}
}