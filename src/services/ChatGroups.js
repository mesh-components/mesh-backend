export class ChatGroups {
	/**
	 * @ngInject
	 */
	constructor(Chat) {
		this.services = {Chat};
	}

	api() {
		const {Chat} = this.services;

		return Chat.service("/chat_groups");
	}
}