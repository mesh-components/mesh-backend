import statusNewIcon from "images/icons/ic_new_ticket.svg";
import statusSignedIcon from "images/icons/ic_signed.svg";
import statusProgressIcon from "images/icons/ic_in_progress.svg";
import statusSolvedIcon from "images/icons/ic_solved.svg";
import statusCloseIcon from "images/icons/ic_closed.svg";
import _ from "main/app.mixins";
import moment from "main/moment-local";

class Tickets {

    /**
     * @ngInject
     */
    constructor($q, Argus, TicketTemplate) {
        this.services = {$q, Argus, TicketTemplate};
        this.statusIcons = {
            new: statusNewIcon,
            signed: statusSignedIcon,
            in_progress: statusProgressIcon,
            solved: statusSolvedIcon,
            closed: statusCloseIcon
        };
        this.apiUrl = "/tickets";

        this.defaultStatusCounts = {
            new: 0,
            signed: 0,
            inProgress: 0,
            solved: 0,
            closed: 0
        };
        const self = this;

        // расширяем модель
        Argus.extendModel(self.apiUrl, (model) => {
            model.getFormattedCreateDate = self.$getFormattedCreateDate;
            model.getFormattedDeadlineDate = self.$getFormattedDeadlineDate;

            return model;
        });
    }

    $getFormattedCreateDate() {
        const date = _.get(this, "createDate", null);

        if (!date) {
            return "";
        }

        return moment(date).local().format("DD/MM/YYYY");
    }

    $getFormattedDeadlineDate() {
        const date = _.get(this, "deadlineDate", null);

        if (!date) {
            return "";
        }

        return moment(date).local().format("DD/MM/YYYY");
    }

    getSolutionDate(date) {
        if (!date) {
            return "";
        }

        return moment(date).local().format("DD/MM/YYYY");
    }

    api(url = null) {
        const {Argus} = this.services;
        const apiUrl = url || this.apiUrl;

        return Argus.service(apiUrl);
    }

    getCategories() {
        return this.api()
            .one("categories", null)
            .getList();
    }

    getTemplate() {
        const {TicketTemplate} = this.services;

        return TicketTemplate.get();
    }

    getStatusCounts(params) {
        return this.api()
            .one("status_counts")
            .get(params)
            .then((counts) => {
                return _.assign({}, this.defaultStatusCounts, counts);
            });
    }

    /**
     * Создает обращение
     * @param data {*}
     * @param captcha {*}
     */
    createTicket(data, captcha = null) {
        const requestData = _.clone(data);
        const url = captcha
            ? `${this.apiUrl}?captcha_id=${captcha.Id}&captcha_value=${captcha.captchaText}`
            : null;

        if (requestData.externalSystemId === 0) {
            requestData.externalSystemId = null;
        }

        return this.api(url).post(requestData);
    }

    /**
     * Загрузка списка опросов
     */
    getPage(num = 1, perPage = 20, params) {
        const completeParams = _.assign(params, {page: num, per_page: perPage});

        return this.api()
            .getList(completeParams)
            .then((response) => {
                const result = {
                    items: response,
                    pageNum: _.int(response.headers("X-Pagination-Page")) || 1,
                    pagesTotal: _.int(response.headers("X-Pagination-Total-Pages")) || 0,
                    itemsTotal: _.int(response.headers("X-Pagination-Total-Items")) || 0
                };

                return result;
            });
    }
}

export {Tickets};