import _ from "main/app.mixins";
import moment from "main/moment-local";

/**
 * Сервис уведомлений
 */
export class TargetMessages {
    /**
     * @ngInject
     */
    constructor(Argus, $interval, News, TargetingCategoryFactory) {
        this.services = {Argus, $interval, News, TargetingCategoryFactory};
        this.fullApi = Argus.withConfig((RestangularConfigurer) => RestangularConfigurer.setFullResponse(true));
        this.url = "/target_messages";
        this.newNoticeCheckingInterval = null;

        // пустое уведомление
        this.EMPTY_NOTICE = {
            title: "",
            detail: "",
            publishDate: "",
            attachments: [],
            // targetTemplate: News.NEWS_TEMPLATE.targetTemplate
            targetTemplate: {
                name: null,
                targetCategories: [
                    TargetingCategoryFactory.get({type: "staff"}),
                    TargetingCategoryFactory.get({type: "parent"}),
                    TargetingCategoryFactory.get({type: "student"})
                ]
            }
        };
    }

    /**
     * на /target_messages
     * @returns {*}
     */
    api() {
        const {Argus} = this.services;

        return Argus.service(this.url);
    }

    customApi(url = this.url) {
        return this.fullApi.all(url);
    }

    /**
     * Запрос на /target_messages/editor
     * @returns {*}
     */
    editorApi() {
        return this.customApi(`${this.url}/editor`);
    }

    /**
     * Запрос на /target_messages/read
     * @returns {*}
     */
    readApi() {
        return this.customApi(`${this.url}/read`);
    }

    /**
     * Получает список непрочитанных уведомлений
     */
    getNoticeUnreadCount() {
        this.api()
            .one("count")
            .get({statuses: "new,viewed"})
            .then((data) => {
                this.count = data.count;
            });
    }

    /**
     * Получает уведомление по id
     * @param id
     */
    getNoticeById(id, params = {}) {
        return this
            .api()
            .one(id)
            .get(params);
    }

    /**
     * Проверяет наличие новых уведомлений
     */
    checkNewNotice() {
        const {$interval} = this.services;

        this.getNoticeUnreadCount();
        this.newNoticeCheckingInterval = $interval(this.getNoticeUnreadCount.bind(this), 60000);
    }

    /**
     * Загружает страницу уведомлений
     * @param page
     * @param perPage
     * @param params
     * @param state
     * @returns {Promise<*>}
     */
    async getPage(page = 1, perPage = 10, params = {}, state) {
        const requestParams = _.assign({}, params, {page, per_page: perPage});

        let response;
        if (state === "table") {
            response = await this
                .editorApi()
                .getList(requestParams);
        } else {
            response = await this
                .customApi()
                .getList(requestParams);
        }

        const result = {
            items: _.get(response, "data", []),
            itemsTotal: _.int(response.headers("X-Pagination-Total-Items")) || 0,
            pageNum: _.int(response.headers("X-Pagination-Page")) || 1,
            pagesTotal: _.int(response.headers("X-Pagination-Total-Pages")) || 0
        };

        return result;
    }

    /**
     * Возвращает статус уведомления
     * @param notice
     * @returns {string}
     */
    getNoticeState(notice) {
        if (!Boolean(notice.id)) {
            return "new";
        }

        const publishDate = moment(_.get(notice, "publishDate", null)).startOf("day") || null;
        const today = moment().startOf("day");

        if (notice.status === "draft" || publishDate.isAfter(today)) {
            return "draft";
        }

        return "published";
    }

    drop() {
        const {$interval} = this.services;
        $interval.cancel(this.newNoticeCheckingInterval);
    }
}