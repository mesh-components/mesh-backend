import _ from "main/app.mixins";

const MESSAGE_TEMPLATE = {
    attachment_ids: [],
    chat_id: null,
    from_profile_id: null,
    text: "",
    read_by: []
};

export class ChatMessages {
    /**
     * @ngInject
     */
    constructor(BigQuery, Chat, CoreAttachments, CookiesStorage, AUTH_TOKEN_FIELD_NAME, AUTH_TOKEN_HEADER_NAME, $interval, UserProfiles) {
        this.services = {BigQuery, Chat, CoreAttachments, CookiesStorage, $interval, UserProfiles};
        this.tokenFieldName = AUTH_TOKEN_FIELD_NAME;
        this.tokenHeaderName = AUTH_TOKEN_HEADER_NAME;
        this.unreadMessagesCount = 0;
        this.unreadInterval = 60 * 1000; // 1 minutes
        this.unreadMessagesInterval = null;
    }

    api() {
        const {Chat} = this.services;

        return Chat.service("/messages");
    }

    /**
     * Загрузка всех сообщений в соответствии с заданными параметрами
     *
     * @param params {*} параметры запроса
     * @param withAttachments {Boolean} параметр, определяющий загружать ли аттачменты
     */
    getAll(params, withAttachments = false) {
        const {BigQuery} = this.services;

        return BigQuery
            .getList(this.api(), params, 50)
            .then((response) => {
                // загружаем аттачи сообщений, если нужно
                if (withAttachments) {
                    return this.$loadAttachmentsForMessages(response);
                }

                return response;
            });
    }

    /**
     * Загрузка конкретной страницы из истории сообщений
     *
     * @param params {*} параметры запроса
     * @param page {Number} номер страницы
     * @param withAttachments {Boolean} параметр, определяющий загружать ли аттачменты
     */
    getPage(params = {}, page = 1, withAttachments = false) {
        const requestParams = _.assign({}, params, {
            page,
            per_page: 50
        });

        return this
            .api()
            .getList(requestParams)
            .then((response) => {
                // загружаем аттачи сообщений, если нужно
                if (withAttachments) {
                    return this.$loadAttachmentsForMessages(response);
                }

                return response;
            });
    }

    prepareMessageText(rawMessageText) {
        const clearObject = document.createElement("div");
        clearObject.innerHTML = rawMessageText;
        const clearText = clearObject.textContent || clearObject.innerText || "";

        return clearText.replace(/(((https?:\/\/)|(ftps?:\/\/)|(www\.))[^\s]+)/ig, `<a href="$1" target="_blank">$1</a>`);
    }

    /**
     * Загрузка аттачей к сообщениям
     *
     * @param messages {Array} масиив сообщений, для которых нужно загрузить аттачи
     */
    $loadAttachmentsForMessages(messages) {
        const {CoreAttachments} = this.services;
        const attachmentsIds = _.chain(messages)
            .map("attachment_ids")
            .flatten()
            .uniq()
            .compact()
            .value();

        if (_.isEmpty(attachmentsIds)) {
            return messages;
        }

        return CoreAttachments
            .api()
            .getList({
                ids: attachmentsIds.join(",")
            })
            .then((response) => {
                return _.map(messages, (message) => {
                    message.attachments = _.filter(response, (attach) => message.attachment_ids.indexOf(attach.id) !== -1);

                    return message;
                });
            });
    }

    $checkUnread() {
        if (this.unreadMessagesInterval) {
            return;
        }

        const {$interval, UserProfiles} = this.services;

        UserProfiles
            .getCurrentProfile()
            .then((profile) => {
                const profileType = _.get(profile, "type", "");
                const notIsArgus = !_.isEmpty(profileType) && profileType !== "argus";

                if (notIsArgus) {
                    this.$loadUnreadCount();
                    this.unreadMessagesInterval = $interval(this.$loadUnreadCount.bind(this), this.unreadInterval);
                }
            });
    }

    /**
     * Загрузка количества непрочитанных сообщений
     */
    $loadUnreadCount() {
        return this
            .api()
            .one("count")
            .get()
            .then((response) => {
                this.unreadMessagesCount = _.get(response, "body", 0);
            });
    }

    /**
     * Очистка сервиса от данных и отмена получения количества непрочитанных сообщений
     */
    drop() {
        const {$interval} = this.services;

        $interval.cancel(this.unreadMessagesInterval);
        this.unreadMessagesCount = 0;
    }

    getTemplate() {
        return _.clone(MESSAGE_TEMPLATE);
    }

    customPUT(message) {
        const {CookiesStorage} = this.services;
        const headers = {
            [this.tokenHeaderName]: CookiesStorage.get(this.tokenFieldName),
            "Profile-Id": CookiesStorage.get("profileId")
        };

        return message.customPUT(null, null, null, headers);
    }
}