import moment from "main/moment-local";

/**
 * уроки доп образования
 */
export class AELessons {
	/**
	 * @ngInject
	 */
	constructor(Core, BigQuery) {
		this.services = {Core, BigQuery};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/ae_lessons");
	}

	getLessons(params) {
		const {BigQuery} = this.services;

		return BigQuery
			.getList(this.api(), params, 50)
			.then((lessons) => {
				return _.map(lessons, (item) => {
					if (item.start_datetime) {
						item.beginDate = moment(item.start_datetime, "YYYY-MM-DD HH:mm:ss"); // начало занятия
						item.endDate = angular.copy(item.beginDate).add(item.duration, "minutes"); // конец занятия
						item.unix = item.beginDate.unix(); // поле для сортировки
					}

					return item;
				});
			});
	}
}