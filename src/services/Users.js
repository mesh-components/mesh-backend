import _ from "main/app.mixins";

class Users {
	/**
	 * @ngInject
	 */
	constructor($q, $interval, Argus, CookiesStorage, UserProfiles, localStorageService) {
		this.services = {$q, $interval, Argus, CookiesStorage, UserProfiles, localStorageService};
		this.currentUser = null;
	}

	api() {
		const {Argus} = this.services;

		return Argus.service("/users");
	}

	/**
	 * Сохраняет в js данные об авторизованном пользователе
	 */
	setCurrentUser(data) {
		const {CookiesStorage, UserProfiles} = this.services;

		CookiesStorage.set("User-Id", data.id);
		UserProfiles.setProfiles(data.profiles);
		delete data.authentication_token;

		// собираем роли из всех профилей пользователей
		data.allRoles = _.chain(data.profiles)
			.map((profile) => UserProfiles.getProfileRoles(profile))
			.flatten()
			.uniq()
			.compact()
			.value();

		// запоминаем id профиля адмнистратора
		const adminProfile = _.find(data.profiles, (profile) => {
			const profileRoles = UserProfiles.getProfileRoles(profile);

			return profile.user_id === data.id && _.intersects(profileRoles, ["admin", "school_admin", "urs_admin"]);
		});

		if (adminProfile) {
			CookiesStorage.set("admin_pid", adminProfile.id);
		}

		this.currentUser = data;
		this.$setUserSettings(data);
	}

	$setUserSettings(userData) {
		const {localStorageService} = this.services;
		const storageKey = `settings.${userData.id}`;

		this.userSettings = localStorageService.get(storageKey);

		if (!this.userSettings) {
			localStorageService.set(storageKey, {
				createdAt: new Date(),
				updatedAt: null,
				disabledModals: []
			});
			this.userSettings = this.getCurrentUserSettings();
		}
	}

	getCurrentUserSettings() {
		const {localStorageService} = this.services;

		return localStorageService.get(`settings.${this.currentUser.id}`);
	}

	updateSetting(field, actionType, value) {
		const {localStorageService} = this.services;
		const currentSettings = this.getCurrentUserSettings();

		switch (actionType) {
			case "add":
				currentSettings[field].push(value);
				break;
			case "remove":
				_.remove(currentSettings[field], (fieldItem) => _.isEqual(fieldItem, value));
				break;
		}

		currentSettings.updatedAt = new Date();
		localStorageService.set(`settings.${this.currentUser.id}`, currentSettings);
	}

	getFullUserName() {
		const firstName = _.get(this.currentUser, "first_name", "");
		const lastName = _.get(this.currentUser, "last_name", "");
		const middleName = _.get(this.currentUser, "middle_name", "");
		const fullName = `${lastName} ${firstName} ${middleName}`;

		return _.trim(fullName);
	}

	/**
	 * Возвращает данные авторизованного пользователя
	 */
	getCurrentUser() {
		const {$q, $interval} = this.services;

		return $q((resolve) => {
			if (_.isEmpty(this.currentUser)) {
				const t = $interval(() => {
					if (!_.isEmpty(this.currentUser)) {
						$interval.cancel(t);
						resolve(this.currentUser);
					}
				}, 200);
			} else {
				resolve(this.currentUser);
			}
		});
	}

	/**
	 * Удаляет из js данные об авторизованном пользователе
	 */
	drop() {
		const {UserProfiles} = this.services;

		UserProfiles.drop();
		this.currentUser = null;
	}
}

export {Users};