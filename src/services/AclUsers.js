import _ from "main/app.mixins";

export class AclUsers {
    /**
     * @ngInject
     */
    constructor($q, $interval, ACL, AUTH_TOKEN_FIELD_NAME, AUTH_TOKEN_HEADER_NAME, CookiesStorage, UserProfiles) {
        this.services = {$q, $interval, ACL, CookiesStorage, UserProfiles};
        this.currentUser = null;
        this.tokenFieldName = AUTH_TOKEN_FIELD_NAME;
        this.tokenHeaderName = AUTH_TOKEN_HEADER_NAME;
    }

    api() {
        const {ACL} = this.services;

        return ACL.service("/users");
    }

    argusProfilesApi() {
        const {ACL} = this.services;
        const rest = ACL.withConfig((RestangularConfigurer) => RestangularConfigurer.setFullResponse(true));

        return rest.service("/profiles/urs");
    }

    /**
     * Загрузка профилей пользователя.
     * Получает данные из ACL
     *
     * @param userId {Number} - id пользователя
     */
    getUserProfiles(userId) {
        const {ACL} = this.services;

        return ACL
            .service(`/users/${userId}/profiles/urs`)
            .getList();
    }

    getProfilesPage(requestParams, page = 1, perPage = 50) {
        const result = {
            data: [],
            pages: 1
        };

        const params = _.assign({}, requestParams, {
            page,
            per_page: perPage
        });

        return this.argusProfilesApi()
            .getList(params)
            .then((response) => {
                result.data = response.data;
                result.pages = _.int(response.headers("X-Pagination-Total-Pages")) || 1;
                result.total_entries = _.int(response.headers("X-Pagination-Total-Entries")) || 0;

                return result;
            });
    }

    createUserProfile(profile) {
        const {ACL} = this.services;

        return ACL
            .service(`/users/${profile.user_id}/profiles`)
            .post(profile);
    }

    createArgusProfile(profile) {
        const {ACL} = this.services;
        return ACL
            .service(`/users/urs/${profile.user_id}/sync`)
            .one().get();
    }

    /**
     * Добавление роли к профилю пользователя
     *
     * @param profileId {number} id профиля
     * @param role {string} название роли
     */
    addRoleToProfile(profileId, role) {
        const {ACL} = this.services;

        return ACL
            .service("/profile_roles")
            .post({
                profile_id: profileId,
                role
            });
    }

    /**
     * Удаление роли из профиля
     *
     * @param profileId {number} id профиля
     * @param role {string} название роли
     */
    removeRoleFromProfile(profileId, role) {
        const {ACL, CookiesStorage} = this.services;
        const headers = {
            [this.tokenHeaderName]: CookiesStorage.get(this.tokenFieldName),
            "Profile-Id": CookiesStorage.get("profileId")
        };
        const params = {
            profile_id: profileId,
            role_name: role
        };

        return ACL
            .all("/profile_roles")
            .customDELETE("", params, headers);
    }

    /**
     * Редактирование роли
     */
    updateRole(role, profileId) {
        const {ACL, CookiesStorage} = this.services;
        const headers = {
            [this.tokenHeaderName]: CookiesStorage.get(this.tokenFieldName),
            "Profile-Id": CookiesStorage.get("profileId")
        };
        const params = {
            profile_id: profileId
        };

        return ACL
            .all(`/urs/roles/${role.id}`)
            .customPUT(role, "", params, headers);
    }
}