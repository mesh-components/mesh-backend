import _ from "main/app.mixins";

class CookiesStorage {
	/**
	 * @ngInject
	 */
	constructor($cookies, $q) {
		this.services = {$cookies, $q};
	}

	/**
	 * Удаление всех cookies
	 */
	drop() {
		const {$q, $cookies} = this.services;

		return $q((resolve) => {
			const allCookies = $cookies.getAll();
			// drop cookies
			_.forOwn(allCookies, (val, k) => {
				$cookies.remove(k);
			});

			const a = setTimeout(() => {
				clearTimeout(a);
				resolve();
			}, 0);
		});
	}

	/**
	 * Удаление конкретного значения cookies
	 * @param itemName {string} название параметра в cookies
	 */
	dropOne(itemName) {
		const {$cookies} = this.services;
		$cookies.remove(itemName);
	}

	/**
	 * Установка значения параметра cookies
	 * @param key {string} название параметра
	 * @param value {*} значение параметра
	 */
	set(key, value) {
		const {$q, $cookies} = this.services;

		return $q((resolve) => {
			$cookies.put(key, value);

			const a = setInterval(() => {
				if (this.get(key) == value) {
					clearInterval(a);
					resolve();
				}
			}, 0);
		});
	}

	/**
	 * Получение значения параметра в cookies
	 * @param key {string} название параметра
	 */
	get(key) {
		const {$cookies} = this.services;

		return $cookies.get(key);
	}
}

export {CookiesStorage};