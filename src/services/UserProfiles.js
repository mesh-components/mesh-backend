import _ from "main/app.mixins";

class UserProfiles {
    /**
     * @ngInject
     */
    constructor($q, $interval, CookiesStorage, AcademicYears, StudentProfiles, ISPPSummary, UrsAlert, Utils) {
        this.services = {$q, $interval, CookiesStorage, AcademicYears, StudentProfiles, ISPPSummary, UrsAlert, Utils};
        this.currentProfile = null;
        this.profiles = [];

        this.profilesTitles = {
            admin: "Администратор",
            school_admin: "Администратор школы",
            school_admin_read_only: "Администратор школы в режиме просмотра",
            principal: "Директор",
            deputy: "Завуч",
            teacher: "Учитель",
            educator: "Воспитатель",
            staff: "Сотрудник",
            passport_printer: "Ответственный за печать аттестатов",
            psychologist: "Педагог-психолог",
            methodologist: "Методист",
            author: "Издатель",
            curator: "Куратор",
            medal_commission: "Медальная комиссия",
            ae_educator: "Преподаватель ДО",
            student: "Учащийся",
            parent: "Родитель"
        };
    }

    /**
     * Сохраняет в сервис список профилей пользователя
     */
    setProfiles(profiles) {
        this.profiles = _.map(profiles, (profile) => {
            profile.roles = this.getProfileRoles(profile);

            return profile;
        });
        this.setDefaultProfile();
    }

    /**
     * Возвращает роли профиля
     */
    getProfileRoles(profile) {
        const roles = _.get(profile, "roles", []);
        const profileType = _.get(profile, "type", "");

        if (profileType && profileType !== "teacher") {
            roles.push(profileType);
        }

        return _.uniq(roles);
    }

    getProfileTitle(profile) {
        if (_.get(profile.roles, "length")) {
            if (_.includes(profile.roles, "urs_admin")) {
                return "Администратор МЭШ";
            } else if (_.includes(profile.roles, "admin")) {
                return "Администратор ОЭЖД";
            } else if (_.includes(profile.roles, "urs_manage_access")) {
                return "Управление ролями в рамках ОО";
            } else if (_.includes(profile.roles, "urs_support")) {
                return "Техническая поддержка";
            } else if (_.includes(profile.roles, "urs_author")) {
                return "Автор контента";
            } else if (_.includes(profile.roles, "student")) {
                return "Учащийся";
            } else if (_.includes(profile.roles, "parent")) {
                return "Родитель";
            } else if (_.includes(profile.roles, "deputy")) {
                return "Завуч";
            } else if (_.includes(profile.roles, "teacher")) {
                return "Учитель";
            } else if (_.includes(profile.roles, "principal")) {
                return "Директор";
            } else if (_.includes(profile.roles, "staff")) {
                return "Сотрудник";
            } else if (_.includes(profile.roles, "school_admin")) {
                return "Администратор школы";
            } else if (_.includes(profile.roles, "educator")) {
                return "Воспитатель";
            } else if (_.includes(profile.roles, "school_admin_read_only")) {
                return "Администратор школы в режиме просмотра";
            } else if (_.includes(profile.roles, "passport_printer")) {
                return "Ответственный за печать аттестатов";
            } else if (_.includes(profile.roles, "psychologist")) {
                return "Педагог-психолог";
            } else if (_.includes(profile.roles, "methodologist")) {
                return "Методист";
            } else if (_.includes(profile.roles, "curator")) {
                return "Куратор";
            } else if (_.includes(profile.roles, "medal_commission")) {
                return "Медальная комиссия";
            }
        }

        return "Пользователь";
    }

    getRoleTitles(profile) {
        const titles = [];
        _.forEach(profile.roles, (role) => {
            titles.push(_.get(this.profilesTitles, role));
        });

        return _.uniq(titles);
    }

    /**
     * Выбор профиля по-умолчанию
     */
    setDefaultProfile() {
        const {CookiesStorage} = this.services;
        const profileId = _.int(CookiesStorage.get("profileId"));

        this.setCurrentProfile(_.find(this.profiles, {id: profileId}) || this.getEZDProfile() || this.profiles[0] || null);
    }

    /**
     * Есть ли у пользователя профиль ЭЖД
     */
    getEZDProfile() {
        return _.find(this.profiles, (profile) => profile.type !== "argus");
    }

    /**
     * Возвращает выбранный профиль
     */
    async getCurrentProfile() {
        const {Utils} = this.services;
        try {
            return await Utils.waitResource(this, "currentProfile");
        } catch (err) {
            throw err;
        }
    }

    /**
     * Выбор указанного профиля
     */
    setCurrentProfile(profile) {
        const {CookiesStorage} = this.services;
        const profileId = _.get(profile, "id");

        this.currentProfile = profile;
        if (profileId) {
            CookiesStorage.set("profileId", profileId);
        }
    }

    /**
     * Загружает список детей родителя
     * */
    async $$setParentsChildren(params) {
        const {StudentProfiles, $q, UrsAlert} = this.services;
        const parentProfile = _.find(this.profiles, {type: "parent"});

        try {
            const studentProfiles = await StudentProfiles.getStudentsByCurrentYear(params);

            parentProfile.children = _.map(studentProfiles, (profile) => {
                const classUnitName = _.get(profile, "class_unit.name", "");
                profile.title = `${profile.short_name} (${classUnitName})`;

                return profile;
            });

            parentProfile.selectedChild = parentProfile.children[0];

            const isppChildrensProms = _.map(parentProfile.children, (child) => this.$$getStudentISPPInfo(child));

            // загружаем информацию по балансу детей
            return await $q.all(isppChildrensProms);
        } catch (err) {
            UrsAlert.handled("Не удалось загрузить список учеников.");
            throw err;
        }
    }

    /**
     * Загружает дополнительные данные о профиле ученика
     * */
    async $$setStudentInfo(params) {
        const {StudentProfiles, UrsAlert} = this.services;
        const studentProfile = _.find(this.profiles, {type: "student"});

        try {
            studentProfile.studentData = await StudentProfiles.getStudentsByCurrentYear(params)[0];

            return await this.$$getStudentISPPInfo(studentProfile);
        } catch (err) {
            UrsAlert.handled("Не удалось загрузить профиль ученика.");
            throw err;
        }
    }

    /**
     * Загружает информацию о балансе прохода и питания ученика
     * */
    async $$getStudentISPPInfo(student) {
        const {ISPPSummary, UrsAlert} = this.services;

        try {
            const isppData = await ISPPSummary.getStudentInfo(student.id);

            isppData.balanceFormatted = ISPPSummary.getFormattedBalance(isppData);
            _.assign(student, {isppData});
        } catch (err) {
            UrsAlert.handled("Не удалось загрузить информацию по балансу прохода и питания ученика.");
        } finally {
            return student;
        }
    }

    /**
     * Устанавливает выбранного ребенка для родителя
     * */
    setSelectedChild(child) {
        /* const {$scope} = this.services;*/

        _.find(this.profiles, {type: "parent"}).selectedChild = child;
        /* $scope.$emit("user-profiles:selected-child-changed");*/
    }

    /**
     * Очистка сохраненных в сервисе данных
     */
    drop() {
        this.currentProfile = null;
        this.profiles = [];
    }

    isStudent() {
        if (!this.currentProfile) {
            return false;
        }

        const roles = _.get(this.currentProfile, "roles", []) || [];

        return _.intersects(roles, ["student"]);
    }

    isParent() {
        if (!this.currentProfile) {
            return false;
        }

        const roles = _.get(this.currentProfile, "roles", []) || [];

        return _.intersects(roles, ["parent"]);
    }

    isStaff() {
        if (!this.currentProfile) {
            return false;
        }

        const roles = _.get(this.currentProfile, "roles", []) || [];

        return _.intersects(roles, ["staff", "teacher", "principal", "deputy", "school_admin", "school_admin_read_only",
            "educator", "psychologist"]);
    }

    async isAdmin() {
        if (!this.currentProfile) {
            await this.getCurrentProfile();
        }

        const roles = _.get(this.currentProfile, "roles", []) || [];

        return _.intersects(roles, ["admin"]);
    }
}

export {UserProfiles};