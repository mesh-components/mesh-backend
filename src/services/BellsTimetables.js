import _ from "main/app.mixins";

/**
 * режим пребывания
 */
export class BellsTimetables {
	/**
	 * @ngInject
	 */
	constructor(Core) {
		this.services = {Core};
		this.bellTypes = [
			{
				id: 1,
				value: "lesson",
				title: "Урок"
			},
			{
				id: 2,
				value: "ec",
				title: "Внеурочная деятельность"
			},
			{
				id: 3,
				value: "aeStudents",
				title: "Доп. образование детей"
			},
			{
				id: 4,
				value: "aeAdults",
				title: "Доп. образование взрослых"
			},
			{
				id: 5,
				value: "gpd",
				title: "ГПД" // группы продленного дня
			},
			{
				id: 6,
				value: "break",
				title: "Перемена"
			},
			{
				id: 7,
				value: "breakfast",
				title: "Завтрак"
			},
			{
				id: 8,
				value: "lunch",
				title: "Обед"
			},
			{
				id: 9,
				value: "snack",
				title: "Полдник"
			},
			{
				id: 10,
				value: "dinner",
				title: "Ужин"
			},
			{
				id: 11,
				value: "physical",
				title: "Физическая активность"
			}
		];
	}

	api() {
		const {Core} = this.services;

		return Core.service("/bells_timetables");
	}

	/**
	 * Возвращает id типа звонка для текстового значения типа
	 *
	 * @param value {String} - текстовый тип звонка
	 *
	 * @returns {*}
	 */
	getBellTypeIdByType(value) {
		const type = _.find(this.bellTypes, {value});

		return _.get(type, "id", null);
	}
}