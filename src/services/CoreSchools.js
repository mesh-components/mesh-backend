export class CoreSchools {
	/**
	 * @ngInject
	 */
	constructor(Core, BigQuery) {
		this.services = {Core, BigQuery};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/schools");
	}
}