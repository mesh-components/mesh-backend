/**
 * @ngInject
 */
function Chat(Restangular, MeschInterceptors) {
	return Restangular.withConfig((RestangularConfigurer) => {
		RestangularConfigurer.setDefaultHeaders({Accept: "application/json"});
		RestangularConfigurer.setBaseUrl("/chat/api");
		RestangularConfigurer.setErrorInterceptor(MeschInterceptors.error.bind(MeschInterceptors));
		RestangularConfigurer.addFullRequestInterceptor(MeschInterceptors.request.bind(MeschInterceptors));
		RestangularConfigurer.addResponseInterceptor(MeschInterceptors.response.bind(MeschInterceptors));
	});
}

export {Chat};