import _ from "main/app.mixins";

/**
 * справочник учебников для для рюкзака школьника
 */
export class StudyBooks {
	/**
	 * @ngInject
	 */
	constructor(Core, $q, BigQuery) {
		this.services = {Core, $q, BigQuery};
		this.items = [];
		this.listIsLoading = false;
	}

	api() {
		const {Core} = this.services;

		return Core.service("/study_books");
	}

	async loadList() {
		const {$q} = this.services;

		// если список не загружается и он пуст, то пробуем загрузить еще раз
		if (!this.listIsLoading && _.isEmpty(this.items)) {
			return await this.$loadBooks();
		}
		// если список загружается, то ждем когда загрузится
		if (this.listIsLoading) {
			return $q((resolve) => {
				const t = setInterval(() => {
					if (!this.listIsLoading) {
						clearInterval(t);
						resolve(angular.copy(this.items));
					}
				}, 200);
			});
		}
		// список не загружается и не пустой

		return $q((resolve) => {
			resolve(angular.copy(this.items));
		});
	}

	async $loadBooks() {
		const {BigQuery} = this.services;

		this.listIsLoading = true;

		try {
			this.items = await BigQuery.getList(this.api(), {}, 50);
		} catch (err) {
			throw err;
		} finally {
			this.listIsLoading = false;
		}

		return this.items;
	}
}