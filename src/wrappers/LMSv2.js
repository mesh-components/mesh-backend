/**
 * @ngInject
 */
function LMSv2(Restangular, MeschInterceptors) {
	return Restangular.withConfig((RestangularConfigurer) => {
		RestangularConfigurer.setDefaultHeaders({Accept: "application/vnd.api.v2+json"});
		RestangularConfigurer.setBaseUrl("/core/api");
		RestangularConfigurer.setErrorInterceptor(MeschInterceptors.error.bind(MeschInterceptors));
		RestangularConfigurer.addFullRequestInterceptor(MeschInterceptors.request.bind(MeschInterceptors));
		RestangularConfigurer.addResponseInterceptor(MeschInterceptors.response.bind(MeschInterceptors));
	});
}

export {LMSv2};