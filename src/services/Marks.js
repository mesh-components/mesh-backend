import _ from "main/app.mixins";
import moment from "main/moment-local";

/**
 * Оценки
 */
export class Marks {
	/**
	 * @ngInject
	 */
	constructor(Core, Restangular, ControlForms) {
		this.services = {Core, Restangular, ControlForms};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/marks");
	}


	/**
	 * Загрузка списка оценок с формами контроля
	 *
	 * @param params {*} - параметры запроса
	 * @param pageSize {Number} - количество оценок на одну страницу в постраничной навигации. По-умолчанию 50
	 * @param withControlForms {Boolean} - флаг определяющий нужно ли загружать формы контроля
	 *
	 * @returns {*}
	 */
	getMarks(params, pageSize = 50, withControlForms = false) {
		// если не просили загружать формы контроля, то просто загрузим оценки
		if (!withControlForms) {
			return this.loadAll(params, pageSize);
		}

		// загружаем оценки с формами котроля
		let marks = [];
		const {ControlForms} = this.services;

		return this
			.loadAll(params, pageSize) // загружаем оценки
			.then((response) => {
				marks = _.reject(response, (item) => !this.isPoint(item) && _.isEmpty(_.get(item, "values[0].grade.origin")));
				// собираем id форм контроля
				const ids = _.chain(response)
					.pluck("control_form_id")
					.compact()
					.uniq()
					.value();

				if (_.isEmpty(ids)) {
					return marks;
				}

				// загружаем формы контроля
				return ControlForms
					.api()
					.getList({
						ids: ids.join(",")
					});
			})
			.then((controlForms) => {
				// присоединяем формы котроля к оценкам
				return _.map(marks, (mark) => {
					mark.controlForm = _.find(controlForms, {id: mark.control_form_id});

					return mark;
				});
			});
	}

	/**
	 * Загрузка всех оценок с учетом параметров запроса
	 *
	 * @param params {*} параметры фильтрации списка оценок
	 * @param pageSize {Number} Количество оценок на одну страницу запроса
	 */
	loadAll(params, pageSize = 50) {
		let page = 1;
		let result = [];
		const self = this;

		return worker();


		function worker() {
			const chunk = _.cloneDeep(params);
			const {Core} = self.services;

			chunk.page = page;
			chunk.per_page = pageSize;

			return Core
				.all("/marks")
				.getList(chunk)
				.then((marks) => {
					const totalPages = _.int(marks.headers("Pages") || marks.headers("X-Pagination-Total-Pages"));
					const {Restangular} = self.services;

					result = _.union(result, marks.plain());

					if (totalPages && !_.isNaN(totalPages) && page === totalPages) {
						return _.map(result, (item) => Restangular.restangularizeElement("", item, marks.getRestangularUrl()));
					}

					page += 1;

					if (_.isEmpty(marks)) {
						return _.map(result, (item) => Restangular.restangularizeElement("", item, marks.getRestangularUrl()));
					}

					return worker();
				});
		}
	}

	/**
	 * Проверка, является ли оценка точкой
	 * @param mark {*} оценка
	 *
	 * @returns {Boolean}
	 */
	isPoint(mark) {
		if (!mark || !mark.is_point) {
			return false;
		}
		const currentDate = moment().startOf("day");
		// в зависимости от режима журнала точка ставится по разному (с указанием даты или без указания)
		const pointDateString = mark.point_date || mark.date;
		const pointDate = moment(pointDateString, "DD.MM.YYYY").startOf("day");

		return currentDate.isBefore(pointDate) || currentDate.isSame(pointDate);
	}

	/**
	 * Возвращает отформатированную строку с начальным и конечным временем точки
	 * */
	getPointFormattedDate(mark){
		const pointStart = moment(mark.created_at, "DD.MM.YYYY HH:mm").format("DD.MM");
		const pointEnd = moment(mark.point_date || mark.date, "DD.MM.YYYY").format("DD.MM");

		return `${pointStart} – ${pointEnd}`;
	}
}