import _ from "main/app.mixins";

class FAQ {
	/**
	 * @ngInject
	 */
	constructor(Argus, BigQuery, TargetingCategoryFactory, TargetCategories) {
		this.services = {Argus, BigQuery, TargetingCategoryFactory, TargetCategories};
		this.targetCategories = TargetCategories.getTargetingCategories() || [];
		this.fullApi = Argus.withConfig(RestangularConfigurer => RestangularConfigurer.setFullResponse(true));
		this.categoriesUrl = "/faq/categories";
		this.url = "/news";
	}

	api() {
		const {Argus} = this.services;

		return Argus.service("/news"); // /faq
	}

	customApi(url = this.url) {
		return this.fullApi.all(url);
	}

	getAll(params={}) {
		const {BigQuery} = this.services;

		return BigQuery.getList(this.api(), params, 100);
	}

	getTemplate() {
		const {TargetingCategoryFactory} = this.services;
		const faqTemplate = {
			externalSystemIds: [], // массив id внешних систем из списка /api/external_systems/available
			categoryId: null, // id категории новости из /api/news/categories
			publishDate: "", // дата публикации новости DD.MM.YYYY
			publishPeriod: 30, // период публикации. количество дней публикации начиная с даты публикации. По-умолчанию 30 дней.
			informationType: "faq",
			title: "", // заголовок новости
			detail: "", // текст новости
			weight: 3, // уровень важности (вес новости)
			targetTemplate: {
				name: null,
				aprilWeight: 1,
				augustWeight: 1,
				decemberWeight: 1,
				februaryWeight: 1,
				januaryWeight: 1,
				julyWeight: 1,
				juneWeight: 1,
				marchWeight: 1,
				mayWeight: 1,
				novemberWeight: 1,
				octoberWeight: 1,
				septemberWeight: 1,
				// массив с категориями охвата, заданными для новости. Для одной новости категорий может быть несколько,
				targetCategories: _.map(this.targetCategories, (category) => TargetingCategoryFactory.get({type: category.params.type}))
			}
		};

		return _.clone(faqTemplate, true);
	}

	/**
	 * Загрузка списка вопросов/ответов
	 */
	getPage(num = 1, perPage, params) {
		const completeParams = _.assign(params, {page: num, per_page: perPage, information_types: "faq"});
		console.log(completeParams)
		return this.fullApi
			.all("/news")
			.getList(completeParams)
			.then((response) => {
				const result = {
					items: response.data,
					pageNum: _.int(response.headers("X-Pagination-Page")) || 1,
					pagesTotal: _.int(response.headers("X-Pagination-Total-Pages")) || 0,
					itemsTotal: _.int(response.headers("X-Pagination-Total-Items")) || 0
				};

				return result;
			});
	}

	/**
	 * Загрузка категорий полезных ссылок
	 *
	 * @returns {Promise.<*>}
	 */
	async getCategories() {
		return await this
			.customApi(this.categoriesUrl)
			.getList()
			.then((response) => response.data)
			.catch(() => []);
	}

	async like(id) {
		return await this.$feedback({id, type: "like"});
	}

	async dislike(id) {
		return await this.$feedback({id, type: "dislike"});
	}

	async $feedback(params) {
		return await this.customApi(this.url + "/feedback").post(params);
	}

	/**
	 * Возвращает
	 * */
	getActuallMonths(item) {
		return _.chain(item.targetTemplate)
			.pick((val, key) => key.includes("Weight") && (val === 5))
			.keys()
			.map((monthKey) => monthKey.replace("Weight", ""))
			.value();
	}

	/**
	 * Возвращает статус вопроса-ответа: черновик, опубликованная
	 */
	getFaqState(faq) {
		if (!Boolean(faq.id)) {
			return "new";
		}
		// если не задана дата публикации, то вопрос-ответ считается черновиком
		if (!Boolean(faq.publishDate)) {
			return "draft";
		}

		const publishDateStart = moment(faq.publishDate).startOf("day");
		const today = moment().startOf("day");

		if (publishDateStart.isBefore(today) || publishDateStart.isSame(today)) {
			return "published";
		}

		// если дата публикации не наступила, то вопрос-ответ считается черновиком

		return "draft";
	}
}

export {FAQ};