/**
 * @ngInject
 */
function Argus(Restangular, MeschInterceptors) {
	return Restangular.withConfig((RestangularConfigurer) => {

		//RestangularConfigurer.setDefaultHttpFields({
		//	timeout: 10000 // ms before cancel
		//})

		RestangularConfigurer.setDefaultHeaders({
			Accept: "application/json",
			"Content-Type": "application/json;charset=utf-8"
		});
		RestangularConfigurer.setBaseUrl("/argus/api");
		RestangularConfigurer.setErrorInterceptor(MeschInterceptors.error.bind(MeschInterceptors));
		RestangularConfigurer.addFullRequestInterceptor(MeschInterceptors.request.bind(MeschInterceptors));
		RestangularConfigurer.addResponseInterceptor(MeschInterceptors.response.bind(MeschInterceptors));
	});
}

export {Argus};