import Moment from "moment";
import "moment/locale/ru";
import { extendMoment } from "moment-range";

Moment.locale("ru");

const moment = extendMoment(Moment);

export default moment;
