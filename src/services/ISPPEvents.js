/**
 * проход и питание
 */
export class ISPPEvents {
	/**
	 * @ngInject
	 */
	constructor(Core) {
		this.services = {Core};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/ispp_events");
	}
}