import _ from "lodash";

_.mixin({int});
_.mixin({float});
_.mixin({intersects});
_.mixin({setCustomErrorMessage});
_.mixin({reverse});
_.mixin({insert});
_.mixin({bool});
_.mixin({mergeElements});
_.mixin({move});
_.mixin({log});
_.mixin({inverse});
_.mixin({findWithIndex});
_.mixin({stringsToObject});

function float(value) {
	if (typeof value === "number") {
		return parseFloat(value);
	}
	const newValue = (value || "").toString().replace(/,/, ".");

	return parseFloat(newValue);
}

function int(value) {
	return parseInt(value, 10);
}

function bool(value) {
	return (value === "false" || value <= 0)? false: Boolean(value);
}

function intersects(...collections) {
	const arrays = _.map(collections, (array) => _.isArray(array) ? array : [array]);
	const intersection = _.intersection.apply(this, arrays);

	return !_.isEmpty(intersection);
}

function setCustomErrorMessage(node, message = "Заполните это поле") {
	node.addEventListener("input", () => {
		node.setCustomValidity("");
	});
	node.addEventListener("click", () => {
		node.setCustomValidity("");
	});
	node.addEventListener("invalid", () => {
		node.setCustomValidity(message);
	});
}

function reverse(arr) {
	return arr.reverse();
}

function insert(arr, index, item) {
	arr.splice(index, 0, item);
}

/**
 * Возвращает смерженный объект из объектов в массиве объектов
 * @param arrayOfObjects: Array<Object>
 *
 * @example
 *  _.mergeElements([{likes: 22}, {dislikes: 12}]) // => {likes: 22, dislikes: 12}
 * */
function mergeElements(arrayOfObjects) {
	const result = {};

	_.forEach(arrayOfObjects, (item) => {
		_.merge(result, item);
	});

	return result;
}

/**
 * Возвращает объект отражающий наличие строк в массиве строк
 * @param arrayOfStrings: Array<String>
 *
 * @example
 *  _.stringsToObject(["select","text"]) // => {select: true, text: true}
 * */
function stringsToObject(arrayOfStrings) {
	const result = {};

	_.forEach(arrayOfStrings, (string) => {
		result[string] = true;
	});

	return result;
}

function move(array, moveIndex, toIndex) {
	/* #move - Moves an array item from one position in an array to another.
	 Note: This is a pure function so a new array will be returned, instead
	 of altering the array argument.
	 Arguments:
	 1. array     (String) : Array in which to move an item.         (required)
	 2. moveIndex (Object) : The index of the item to move.          (required)
	 3. toIndex   (Object) : The index to move item at moveIndex to. (required)
	 */
	let item = array[moveIndex];
	let length = array.length;
	let diff = moveIndex - toIndex;

	if (diff > 0) {
		// move left
		return [
			...array.slice(0, toIndex),
			item,
			...array.slice(toIndex, moveIndex),
			...array.slice(moveIndex + 1, length)
		];
	} else if (diff < 0) {
		// move right
		return [
			...array.slice(0, moveIndex),
			...array.slice(moveIndex + 1, toIndex + 1),
			item,
			...array.slice(toIndex + 1, length)
		];
	}

	return array;
}

/**
 * Выводит console.log если выполняется условие, переданное в первом аргументе
 * @param statement:any - условие
 * */
function log(statement, ...args) {
	if (statement) {
		console.log(...args);
	}
}


/**
 * Применяет логическое отрицание к аргументу
 * (запилено чтобы юзать в _.chain)
 * */
function inverse(value) {
	return !value;
}

/**
 * Ищет и возвращает найденную сущность с ее индексом в массиве [entity, ix]
 * @param collection: Array - коллекция, в которой осуществляется поиск
 * @param statement: Object | Function - условие поиска
 *
 * @example
 * _.findWithIndex([{id: 1, name: 'User'}], {id: 1}) // => [{id: 1, name: 'User'}, 0]
 * */
function findWithIndex(collection, statement) {
	const ix = _.findIndex(collection, statement);

	return [collection[ix], ix];
}

export default _;