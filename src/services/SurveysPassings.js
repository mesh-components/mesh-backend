import _ from "main/app.mixins";
import moment from "main/moment-local";

class SurveysPassings {
    /**
     * @ngInject
     */
    constructor(Survey, BigQuery, FileSaver) {
        this.services = {Survey, BigQuery, FileSaver};
        this.fullApi = Survey.withConfig((RestangularConfigurer) => RestangularConfigurer.setFullResponse(true));
        this.url = "/survey/statistics";

        this.passingTemplate = {
            surveyId: null,
            duration: null,
            date: null,
            answers: []
        };

        this.answerTemplate = {
            questionId: null,
            variantsIds: [],
            answerText: null
        };
    }

    api() {
        const {Survey} = this.services;

        return Survey.service(this.url);
    }

    customApi(url = this.url) {
        return this.fullApi.all(url);
    }

    getAll(params = {}) {
        const {BigQuery} = this.services;

        return BigQuery.getList(this.api(), params, 100);
    }


    getTemplate(surveyId = null) {
        const result = _.clone(this.passingTemplate, true);
        result.surveyId = surveyId;

        return result;
    }

    getAnswerTemplate(types = ["select"], questionId = null) {
        const result = _.clone(this.answerTemplate, true);

        result.questionId = questionId;

        if (!_.includes(types, "select")) {
            delete result.variantsIds;
        }

        return result;
    }

    downloadSurveyXLS(surveyId) {
        window.open(`survey/api${this.url}/download/${surveyId}`);
    }

    downloadQuestionXLS(questionId) {
        window.open(`survey/api${this.url}/question/download/${questionId}`);
    }
}

export {SurveysPassings};