/**
 * Оценки доп образования
 */
export class AEMarks {
	/**
	 * @ngInject
	 */
	constructor(Core, BigQuery) {
		this.services = {Core, BigQuery};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/ae_marks");
	}

	/**
	 * Загрузка списка оценок
	 *
	 * @param params {*} - параметры запроса
	 * @returns {*}
	 */
	loadAll(params) {
		const {BigQuery} = this.services;

		return BigQuery.getList(this.api(), params, 50);
	}
}