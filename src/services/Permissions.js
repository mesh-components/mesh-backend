import _ from "main/app.mixins";

export class Permissions {
    /**
     * @ngInject
     */
    constructor(UserProfiles, ACL) {
        this.services = {UserProfiles, ACL};
        this.roles = [
            {
                title: "Администратор системы",
                role: "urs_admin",
                bySchools: false,
                availableFeatures: []
            },
            {
                title: "Техническая поддержка",
                role: "urs_support",
                bySchools: false,
                availableFeatures: ["AddAndUpdateBypass", "AddAndUpdateFaq"]
            },
            {
                title: "Автор контента",
                role: "urs_author",
                bySchools: true,
                availableFeatures: [
                    "AddAndUpdateNews",
                    "AddAndUpdateEvent",
                    "AddAndUpdateUsefulLinks",
                    "AddAndUpdateTargetMessages",
                    "AddAndUpdateTargetTemplates",
                    "AddAndUpdateSurvey"
                ]
            },
            {
                title: "Управление ролями в рамках ОО",
                role: "urs_manage_access",
                bySchools: true,
                availableFeatures: [
                    "AddAndUpdateRolesBySchools"
                ]
            }
        ];
        this.features = [
            {
                title: "Администрирование раздела «помощь»",
                value: "AddAndUpdateFaq",
                description: "Создание, редактирование, публикация, снятие с публикации, удаление вопросов/ответов"
            },
            {
                title: "Публикация обходных решений",
                value: "AddAndUpdateBypass",
                description: "Создание, редактирование, публикация, снятие с публикации обходных решений"
            },
            {
                title: "Публикация новостей",
                value: "AddAndUpdateNews",
                description: "Создание, редактирование, публикация, снятие с публикации новостей"
            },
            {
                title: "Публикация событий",
                value: "AddAndUpdateEvent",
                description: "Создание, редактирование, публикация, снятие с публикации новостей"
            },
            {
                title: "Публикация полезных ссылок",
                value: "AddAndUpdateUsefulLinks",
                description: "Создание, редактирование, публикация, удаление полезных ссылок"
            },
            {
                title: "Публикация таргетированных нотификаций",
                value: "AddAndUpdateTargetMessages",
                description: "Создание, редактирование, публикация таргетированных нотификаций"
            },
            {
                title: "Управление шаблонами настроек целевой аудитории",
                value: "AddAndUpdateTargetTemplates",
                description: "Создание, редактирование шаблонов и групп шаблонов"
            },
            {
                title: "Управление правами доступа",
                value: "AddAndUpdateRolesBySchools",
                description: "Управление ролями/правами доступа"
            },
            {
                title: "Публикация опросов",
                value: "AddAndUpdateSurvey",
                description: "Создание, редактирование, публикация, удаление опросов"
            }
        ];
    }

    /**
     * Проверка есть ли у текущего пользователя определенные роли
     *
     * @param needRoles {Array} массив ролей/фич, наличие которых нужно проверить
     *
     * @returns {Promise.<*>}
     */
    checkPermissions(needRoles) {
        const {UserProfiles} = this.services;

        return UserProfiles
            .getCurrentProfile()
            .then((profile) => this.hasRolesFeatures(needRoles, profile));
    }

    /**
     * Проверка наличия набора ролей/фич в конкретном профиле
     *
     * @param check {Array} массив ролей/фич, наличие которых нужно проверить
     * @param profile {*} профиль, для которого нужно проверить наличие ролей/фич
     *
     * @returns {*}
     */
    hasRolesFeatures(check, profile) {
        const {UserProfiles} = this.services;
        const roles = UserProfiles.getProfileRoles(profile);

        // проверяем и роли и фичи
        const ursRoles = _.chain(profile.urs_roles)
            .map((role) => _.union([role.name], role.urs_features))
            .flatten()
            .compact()
            .uniq()
            .value();

        return _.intersects(_.union(roles, ursRoles), _.isArray(check) ? check : [check]);
    }

    getRoles() {
        const {ACL} = this.services;

        return ACL.service("/acl_roles").getList();
    }
}