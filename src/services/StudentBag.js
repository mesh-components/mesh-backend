/**
 * рюкзак школьника
 */
export class StudentBag {
	/**
	 * @ngInject
	 */
	constructor(Core) {
		this.services = {Core};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/student_bag");
	}
}