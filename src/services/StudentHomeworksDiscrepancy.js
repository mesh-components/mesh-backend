/**
 * Сообщения о несоответствии времени выполнения дз
 */
export class StudentHomeworksDiscrepancy {
	/**
	 * @ngInject
	 */
	constructor(Core) {
		this.services = {Core};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/student_homeworks_discrepancy");
	}

	getTemplate() {
		return {
			student_homework_id: null, // id сущности из /api/student_homeworks
			real_duration: null, // количество минут, реально (со слов ученика или родителя) потраченных на выполнение дз
			student_profile_id: null, // id профиля ученика
			profile_id: null // id профиля пользователя, сообщившего о несоответствии
		};
	}

	/**
	 * Возвращает продолжительность домашней работы
	 *
	 * @returns {string}
	 */
	getHumanDuration() {
		let displayDuration;

		if (this.real_duration < 60) {
			return `${this.real_duration || 0} мин`;
		}

		if (this.real_duration >= 60 && this.real_duration < 1440) {
			displayDuration = _.floor(this.real_duration / 60, 2);

			return `${displayDuration || 0} ч`;
		}

		if (this.real_duration >= 1440) {
			displayDuration = _.floor(this.real_duration / 1440, 2);

			return `${displayDuration || 0} д`;
		}

		return "";
	}
}