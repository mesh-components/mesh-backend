/**
 * Учебные планы
 */
export class ParallelCurricula {
	/**
	 * @ngInject
	 */
	constructor(Jersey) {
		this.services = {Jersey};
	}

	api() {
		const {Jersey} = this.services;

		return Jersey.service("/parallel_curricula");
	}
}