import _ from "main/app.mixins";

export class NewsFilter {
	/**
	 * @ngInject
	 */
	constructor($rootScope) {
		this.services = {$rootScope};
		/**
		 * По категориям таргетинга: архив, для учащихся и т.д.
		 */
		this.targetingParams = {
			type: "all"
		};
		/**
		 * По типу информации: Новости, События, Обходные решения
		 */
		this.informationType = [];
		/**
		 * По информационным категориям: Официальная, Служебная, Полезная, Занимательная
		 */
		this.categories = [];
		/**
		 * По внешним системам
		 */
		this.externalSystems = [];
		/**
		 * По источнику
		 */
		this.source = null;
		/**
		 *Режим отображения новостей
		 */
		this.viewType = "tile";
	}

	/**
	 * Геттер/Сеттер для типа отображения списка новостей
	 */
	viewTypeGetterSetter(mode) {
		if (mode) {
			this.viewType = mode;
		}

		return this.viewType;
	}

	/**
	 * Получение параметров фильтрации
	 */
	getRequestParams() {
		let params = {};

		params = _.assign(params, this.$getTargeting());
		params = _.assign(params, this.$getExternalSystems());
		params = _.assign(params, this.$getInformationTypes());
		params = _.assign(params, this.$getSource());
		params = _.assign(params, this.$getCategories());

		return params;
	}

	/**
	 * Бродкаст фильтров новостей
	 */
	applyFilters() {
		const {$rootScope} = this.services;
		$rootScope.$broadcast("NewsFilter:apply", {params: this.getRequestParams()});
	}

	drop() {
		this.targetingParams = {};
		this.informationType = [];
		this.categories = [];
		this.externalSystems = [];
		this.source = null;
	}

	$getCategories() {
		if (_.isEmpty(this.categories)) {
			return {};
		}

		const ids = _.chain(this.categories)
			.pluck("id")
			.compact()
			.value();

		return {
			category_ids: ids.join(",")
		};
	}

	$getInformationTypes() {
		if (_.isEmpty(this.informationType)) {
			return {};
		}

		return {
			information_types: this.informationType.join(",")
		};
	}

	$getSource() {
		if (!this.source) {
			return {};
		}

		return {
			source: this.source
		};
	}

	$getExternalSystems() {
		if (_.isEmpty(this.externalSystems)) {
			return {};
		}

		const ids = _.chain(this.externalSystems)
			.pluck("id")
			.compact()
			.value();

		return ids.length === 0 ? {} : {external_system_ids: ids.join(",")};
	}

	$getTargeting() {
		if (_.isEmpty(this.targetingParams)) {
			return {};
		}
		const params = angular.copy(this.targetingParams);
		const {type} = params;

		delete params.type;

		if (type !== "archive" && type !== "all") {
			params.target_category = type;
		}

		return params;
	}
}