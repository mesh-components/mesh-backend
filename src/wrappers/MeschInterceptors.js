import _ from "main/app.mixins";
import md5 from "npm/md5";

class MeschInterceptors {

    /**
     * @ngInject
     */
    constructor($injector, AUTH_TOKEN_FIELD_NAME, AUTH_TOKEN_HEADER_NAME, CookiesStorage, Errors, UrsAlert) {
        this.services = {$injector, CookiesStorage, Errors, UrsAlert};
        this.tokenFieldName = AUTH_TOKEN_FIELD_NAME;
        this.tokenHeaderName = AUTH_TOKEN_HEADER_NAME;
    }

    error(res, defer) {
        const {$injector, UrsAlert, CookiesStorage, Errors} = this.services;

        defer.reject(res.data);

        switch (res.status) {
            case 500:
                UrsAlert.error("На сервере произошла непредвиденная ошибка.<br />Повторите запрос позже.", true);

                return false;

            case 401:
                if (CookiesStorage.get(this.tokenFieldName)) {
                    const MeschSessions = $injector.get("MeschSessions");
                    // кончилось время жизни сессии
                    MeschSessions.drop();
                    UrsAlert.error("Авторизационные данные устарели.<br />Введите логин и пароль еще раз.", true);

                    return false;
                }
                UrsAlert.error("Учетная запись не найдена.<br />Проверьте правильность ввода<br />логина и пароля.", true);

                return false;

            case 404:
                UrsAlert.error("Запрошенные данные отсутствуют.<br />Повторите запрос позже или напишите нам.", true);

                return false;

            case 403:
                UrsAlert.error("Нет доступа к запрашиваемым данным<br />Повторите запрос позже или напишите нам.", true);

                return false;

            default:
                const errorCode = _.get(res.data, "errorCode", null);
                const errorMessage = _.get(res.data, "errorMessage", "");

                if (!errorCode) {
                    if (errorMessage.indexOf("Incorrect captcha") !== -1) {
                        UrsAlert.error("Неверно введены символы с картинки.<br />Попробуйте еще раз", true);
                    }
                    console.error(`Запрос вернул ошибку ${errorMessage}`, res);

                    return true;
                }
                UrsAlert.error(Errors.get(errorCode));

                return false;
        }

        return true;
    }

    request(element, operation, route, url, headers) {
        const {CookiesStorage} = this.services;

        const token = CookiesStorage.get(this.tokenFieldName);

        if (url.includes("/argus")) {
            headers["X-Log-Id"] = md5(token || "");
        }

        headers[this.tokenHeaderName] = token;
        headers["Profile-Id"] = CookiesStorage.get("profileId");

        if (operation === "remove") {
            headers["Content-Type"] = "application/json;charset=utf-8";

            return {
                headers,
                element: null
            };
        }

        return {
            headers,
            element
        };
    }

    response(data, operation, what, url, response, deferred) {
        if (data.errorCode || data.errorMessage) {
            this.error(response, deferred);
        }
        // ошибка МЭШ
        if (data.errorCode) {
            return data;
        }
        // ответ МЭШ
        if (data.results) {
            return data.results;
        }
        // ответ ЭЖД
        let $data = data;

        if (!_.isObject($data)) {
            $data = {
                body: $data
            };
        }

        $data.headers = response.headers;

        return $data;
    }

}

export {MeschInterceptors};