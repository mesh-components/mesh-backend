/**
 * Параллели
 */
class ClassLevels {
	/**
	 * @ngInject
	 */
	constructor(Argus, BigQuery) {
		this.services = {Argus, BigQuery};
	}

	api() {
		const {Argus} = this.services;

		return Argus.service("/school_dictionaries/class_levels");
	}

	getAll(params={}) {
		const {BigQuery} = this.services;

		return BigQuery.getList(this.api(), params, 20);
	}
}

export {ClassLevels};