/**
 * Сервис настроек таблиц черновиков, опубликованных, архивов
 * в новостях, полезных ссылках, опросах, уведомлениях, вопросах-ответах
 */
export class UrsTableColumnSettings {
    /**
     * @ngInject
     */
    constructor(UserProfiles, $stateParams, $state, localStorageService) {
        this.services = {UserProfiles, $stateParams, $state, localStorageService};

        // настройки по умолчанию для всех таблиц
        this.columnSettings = {

            // ссылки
            links: {
                published: {
                    show: [1, 1, 1, 1, 1, 1, 1, 1, 0],
                    available: [0, 1, 1, 1, 1, 1, 1, 1, 1]
                },
                draft: {
                    show: [1, 1, 1, 1, 1, 1, 0, 1, 0],
                    available: [0, 1, 1, 1, 1, 1, 0, 1, 1]
                }
            },

            // новости
            news: {
                published: {
                    show: [1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0],
                    available: [1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
                },
                draft: {
                    show: [1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0],
                    available: [1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1]
                },
                archive: {
                    show: [1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0],
                    available: [1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
                }
            },

            // вопросы-ответы
            support: {
                published: {
                    show: [1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
                    available: [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
                },
                draft: {
                    show: [1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0],
                    available: [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1]
                }
            },

            // опросы
            surveys: {
                draft: {
                    show: [1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0],
                    available: [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0]
                },
                published: {
                    show: [1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1],
                    available: [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
                },
                archive: {
                    show: [1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1],
                    available: [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
                }
            },

            // уведомления
            target_messages: {
                published: {
                    show: [1, 1, 0, 1, 1, 1, 1, 1, 1, 0],
                    available: [0, 1, 1, 1, 1, 1, 1, 1, 1, 1]
                },
                draft: {
                    show: [1, 1, 0, 1, 1, 1, 1, 1, 1, 0],
                    available: [0, 1, 1, 1, 1, 1, 1, 1, 1, 1]
                }
            }
        };
    }

    /**
     * Получает настройки колонок таблиц для каждого пользователя
     * @returns {*}
     */
    getColumnSettings() {
        const {$state} = this.services;

        const stateName = $state.current.name.match(/[a-z_]+/);
        const defaultSettings = this.columnSettings[stateName[0]];

        const storageObject = this.getStorageObject();
        const storageSettings = _.get(storageObject, `adminTable.${stateName[0]}`, {});

        if (_.isEmpty(storageSettings)) {
            return defaultSettings;
        }

        _.forEach(storageSettings, (val, key) => {
            if (val.show.length !== defaultSettings[key].show.length) {
                storageSettings[key] = defaultSettings[key];
            }
        });

        return storageSettings;
    }

    /**
     * Записывает в localStorage настройки таблицы из шестеренки
     */
    writeToLocalStorage(userSettings) {
        const {$stateParams, UserProfiles, $state, localStorageService} = this.services;

        const stateName = $state.current.name.match(/[a-z_]+/);
        const columnSettings = this.getColumnSettings();

        columnSettings[$stateParams.type].show = userSettings;

        const storageObject = this.getStorageObject();

        // проверяем, есть ли в объекте поле adminTable
        const adminTable = _.get(storageObject, "adminTable", {});
        if (_.isEmpty(adminTable)) {
            storageObject.adminTable = {};
        }
        storageObject.adminTable[stateName[0]] = columnSettings;

        if (localStorageService.isSupported) {
            localStorage.setItem(`mesh.settings.${UserProfiles.currentProfile.id}`, JSON.stringify(storageObject));
        }
    }

    /**
     * Получает объект из localStorage
     * @returns {*}
     */
    getStorageObject() {
        const {UserProfiles, localStorageService} = this.services;

        if (!localStorageService.isSupported) {
            return {};
        }

        const localStorageKey = `mesh.settings.${UserProfiles.currentProfile.id}`;
        if (_.isEmpty(localStorage.getItem(localStorageKey))) {
            return {};
        }

        try {
            return JSON.parse(localStorage.getItem(localStorageKey));
        } catch (err) {
            return {};
        }
    }
}