import _ from "main/app.mixins";

class EducationLevels {

	/**
	 * @ngInject
	 */
	constructor(Argus, BigQuery) {
		this.services = {Argus, BigQuery};
	}

	api() {
		const {Argus} = this.services;

		return Argus.service("/school_dictionaries/education_levels");
	}

	getAll() {
		const {BigQuery} = this.services;

		return BigQuery.getList(this.api(), {}, 20);
	}
}

export {EducationLevels};