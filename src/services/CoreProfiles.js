import _ from "main/app.mixins";

export class CoreProfiles {
	/**
	 * @ngInject
	 */
	constructor(Core, BigQuery) {
		this.services = {Core, BigQuery};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/profiles");
	}

	getAll(params) {
		const {BigQuery} = this.services;

		return BigQuery.getList(this.api(), params, 50);
	}

	async getPage(page, filter) {
		const {Core} = this.services;
		const result = {
			data: [],
			pages: 1
		};
		const rest = Core.withConfig((RestangularConfigurer) => RestangularConfigurer.setFullResponse(true));
		let params = {page, per_page: 50};

		if (_.keys(filter).length !== 0) {
			params = angular.extend(params, filter);
		}

		return await rest.all("/profiles")
			.getList(params)
			.then((response) => {
				result.data = response.data;
				result.pages = _.int(response.headers("X-Pagination-Total-Pages")) || 1;
				result.total_entries = _.int(response.headers("X-Pagination-Total-Entries")) || 0;

				return result;
			});
	}
}