/**
 * Учебные группы учеников
 */
export class StudentGroups {
	/**
	 * @ngInject
	 */
	constructor(Jersey) {
		this.services = {Jersey};
	}

	api() {
		const {Jersey} = this.services;

		return Jersey.service("/groups");
	}
}