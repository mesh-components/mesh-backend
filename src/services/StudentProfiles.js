import _ from "main/app.mixins";

/**
 * Профили учеников
 */
export class StudentProfiles {
    /**
     * @ngInject
     */
    constructor(Core, AcademicYears, BigQuery) {
        this.services = {Core, AcademicYears, BigQuery};
    }

    api() {
        const {Core} = this.services;

        return Core.service("/student_profiles");
    }

    getAll(params) {
        const {BigQuery} = this.services;

        return BigQuery.getList(this.api(), params, 50);
    }

    getPage(filter, page = 1, perPage = 50) {
        const {Core} = this.services;
        const result = {
            data: [],
            pages: 1
        };
        const rest = Core.withConfig((RestangularConfigurer) => RestangularConfigurer.setFullResponse(true));
        let params = {
            page,
            per_page: perPage
        };

        if (_.keys(filter).length !== 0) {
            params = angular.extend(params, filter);
        }

        return rest.all("/student_profiles")
            .getList(params)
            .then((response) => {
                result.data = response.data;
                result.pages = _.int(response.headers("X-Pagination-Total-Pages")) || 1;
                result.total_entries = _.int(response.headers("X-Pagination-Total-Entries")) || 0;

                return result;
            });
    }

    /**
     * Проверка является ли переданный профиль профилем ученика
     */
    isStudent(profile) {
        const profileRoles = _.get(profile, "roles", []);

        return _.intersects(profileRoles, "student");
    }

    getStudentsByCurrentYear(params) {
        const {AcademicYears} = this.services;

        return AcademicYears
            .getSelected()
            .then((year) => {
                const requestParams = _.assign({academic_year_id: _.get(year, "id", null)}, params);

                return this
                    .api()
                    .getList(requestParams);
            });
    }
}