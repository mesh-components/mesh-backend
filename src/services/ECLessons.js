import moment from "main/moment-local";

/**
 * занятия внеурочной деятельности
 */
export class ECLessons {
	/**
	 * @ngInject
	 */
	constructor(Core) {
		this.services = {Core};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/ec_lessons");
	}

	/**
	 * Загрузка списка уроков
	 *
	 * @param params {*} - параметры запроса
	 * @returns {Promise}
	 */
	getLessons(params = {}) {
		return this
			.api()
			.getList(params)
			.then((lessons) => {
				return _.map(lessons, (item) => {
					if (item.real_date) {
						item.beginDate = moment(`${item.real_date} ${item.real_time}`, "DD.MM.YYYY HH:mm"); // начало занятия
						item.endDate = angular.copy(item.beginDate).add(_.int(item.duration), "minutes"); // конец занятия
						item.unix = item.beginDate.unix(); // поле для сортировки
					}

					return item;
				});
			});
	}
}