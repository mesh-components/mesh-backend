/**
 * баланс прохода и питания
 */
export class ISPPSummary {
	/**
	 * @ngInject
	 */
	constructor(Core, $uibModal) {
		this.services = {Core, $uibModal};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/ispp_summary");
	}

	/**
	 * Возвращает информацию по балансу ученика
	 *
	 * @param studentId {Number} - id профиля ученика
	 */
	getStudentInfo(studentId) {
		return this.api()
			.one(studentId)
			.get();
	}

	/**
	 * Форматирование строки с балансом ученика.
	 *
	 * @param isppData {*} объект с ispp данными ученика
	 * @returns {String} отформатированная строка, в которой разряды разделены пробелами (9999999 => 9 999 999)
	 */
	getFormattedBalance(isppData) {
		const balance = _.get(isppData, "balance", null);

		if (!balance) {
			return "0";
		}

		return _(balance).toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1 ");
	}

	/**
	 * Открываем модальное окно с ispp информацией по балансу
	 */
	openIsppInfo(child, modalAppendTo = null) {
		const {$uibModal} = this.services;

		$uibModal
			.open({
				animation: true,
				appendTo: modalAppendTo,
				component: "isppInfoDetail",
				size: "md",
				resolve: {
					isppData: () => child.isppData,
					student: () => child
				}
			});
	}
}