import _ from "main/app.mixins";
import moment from "main/moment-local";

class PeriodSchedule {
	/**
	 * @ngInject
	 */
	constructor(Core, $q) {
		this.services = {Core, $q}
		this.url = "/periods_schedules";
		this.collection = Core.all(this.url);
		this.collection.getList = (query) => Core.all(this.url).getList(query);
		this.one = (id) => Core.one(this.url, id);
	}

	/**
	 *
	 * @param id
	 * @returns {*}
	 */
	 bindOne(id) {
		const {Core} = this.services;

		return Core
			.one(this.url, id)
			.get()
			.then((data) => {
				data.periods = data.periods || [];
				Core.restangularizeCollection(data, data.periods, "periods");

				return data;
			});
	}

	getOne(id) {
		const {Core, $q} = this.services;
		const deferred = $q.defer();

		if (!id) {
			deferred.reject();

			return deferred.promise;
		}

		return Core
			.one(this.url, id)
			.get()
			.then((data) => {
				if (data && (!data.errCode || data.errCode === 0)) {
					return data;
				}

				throw data;
			});
	}

	getPeriods(id) {
		const {Core, $q} = this.services;
		const deferred = $q.defer();

		if (!id) {
			deferred.reject();

			return deferred.promise;
		}

		return Core
			.one(this.url, id)
			.all("periods")
			.getList()
			.then((data) => {
				if (data && (!data.errCode || data.errCode === 0)) {
					return data;
				}

				throw data;
			});
	}

	/**
	 * Возвращает текущий учебный период.
	 * Если каникулы, то предыдущий
	 * @param periods периоды
	 */
	getCurrentStudyPeriod(periods) {
		let currentPeriod;
		let prevPeriod;
		periods.sort((lhs, rhs) => {
			return lhs.begin_date > rhs.begin_date ?
				1 :
				lhs.begin_date < rhs.begin_date ?
					-1 :
					0;
		});

		_.forEach(periods, period => {
			if (moment().isBetween(moment(period.begin_date).subtract(1, "days"), moment(period.end_date).add(1, "days"))) {
				// если каникулы
				if (period.is_vacation) {
					currentPeriod = prevPeriod;
				} else {
					currentPeriod = period;
				}

				return false;
			}
		});

		if (!currentPeriod) {
			console.error("Не найден учебный период");
		}

		return currentPeriod;
	}

	/**
	 * Копирование ГУП
	 * @param plan {Object}
	 */
	copy(plan) {
		const {$q} = this.services;
		const deferred = $q.defer();

		if (!plan.id) {
			deferred.reject({message: "Копирование невозможно"});

			return deferred.promise;
		}

		return this.collection.customPOST({}, plan.id + "/copy");
	}
}

export {PeriodSchedule};
