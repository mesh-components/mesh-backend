class CoreClassUnits {

	/**
	 * @ngInject
	 */
	constructor(Core) {
		this.services = {Core};
	}

	api() {
		const {Core} = this.services;

		return Core.service("/class_units");
	}
}

export {CoreClassUnits};